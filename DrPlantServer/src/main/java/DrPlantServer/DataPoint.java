package DrPlantServer;

class DataPoint {
	private String type;
	private String task;
	private String value;
	private long time;
	
	public DataPoint(String type, String task, String value, long time) {
	    this.type = type;
	    this.task = task;
		this.value = value;
		this.time = time;
	}
	
	public String getValue() {
		return value;
	}
	
	public long getTime() {
		return time;
	}
	
	public String getTask() {
	    return task;
	}
	
	public String getType() {
        return type;
    }
}
