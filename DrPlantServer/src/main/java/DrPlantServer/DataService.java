package DrPlantServer;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/*
 * Data Service as a vertx event-loop 
 */
public class DataService extends AbstractVerticle {

	private int port;
	private static final int MAX_SIZE = 100;
	private Map<Integer, LinkedList<DataPoint>> values;
	private Map<Integer, LinkedList<OptimalValue>> optimalValues;
	private Integer numId;
	
	public DataService(int port) {
		this.values = new HashMap<>();	
		this.optimalValues = new HashMap<>();
		this.port = port;
		this.numId = 0;
	}

	@Override
	public void start() {		
		Router router = Router.router(vertx);
		router.route().handler(BodyHandler.create());
		router.get("/api/data/id").handler(this::handleDeviceId);
		router.get("/api/data/idConnected/").handler(this::handleDeviceIdConnected);
		router.post("/api/data/").handler(this::handleAddNewData);
		router.get("/api/data/").handler(this::handleGetData);		
	    router.post("/api/data/optimalValue/").handler(this::handleAddOptimalValue);
	    router.get("/api/data/optimalValue/").handler(this::handleGetOptimalValue);      
		vertx
			.createHttpServer()
			.requestHandler(router)
			.listen(port);

		log("Service ready.");
	}
	
	private void handleDeviceId(RoutingContext routingContext) {
	    this.numId++;
	    this.values.put(this.numId, new LinkedList<>());
	    
	    JsonObject data = new JsonObject();
        data.put("id", this.numId);
        
	    routingContext.response()
        .putHeader("content-type", "application/json")
        .end(data.encodePrettily());
	}
	
	private void handleDeviceIdConnected(RoutingContext routingContext) {
        this.values.put(this.numId, new LinkedList<>());
        
        JsonObject data = new JsonObject();
        data.put("id", this.numId);
        
        routingContext.response()
        .putHeader("content-type", "application/json")
        .end(data.encodePrettily());
        log("send id=" + numId);
	}
	
	private void handleAddNewData(RoutingContext routingContext) {
		HttpServerResponse response = routingContext.response();
		// log("new msg "+routingContext.getBodyAsString());
		JsonObject res = routingContext.getBodyAsJson();
		if (res == null) {
			sendError(400, response);
		} else {
			String type = res.getString("type");
			String task = res.getString("task");
	        String value = res.getString("value");
	        String id = res.getString("id");
			long time = System.currentTimeMillis();
			
			values.get(Integer.parseInt(id)).addFirst(new DataPoint(type, task, value, time));
			if (values.size() > MAX_SIZE) {
				values.get(Integer.parseInt(id)).removeLast();
			}
			
			log("task: " + task + " type: " + type + "value: " + value + " on " + new Date(time));
			response.setStatusCode(200).end();
		}
	}
	
	private void handleGetData(RoutingContext routingContext) {
	    HttpServerRequest request = routingContext.request();
	    MultiMap params =  request.params();
	    Integer id = Integer.parseInt(params.get("id"));
	    
		JsonArray arr = new JsonArray();
		for (DataPoint p: values.get(id)) {
			JsonObject data = new JsonObject();
			data.put("type", p.getType());
			data.put("task", p.getTask());
			data.put("value", p.getValue());
			arr.add(data);
		}
		values.get(id).clear();
		routingContext.response()
			.putHeader("content-type", "application/json")
			.end(arr.encodePrettily());
	}
	
	   private void handleAddOptimalValue(RoutingContext routingContext) {
	       System.out.println("handleAddOptimalValue");
	        HttpServerResponse response = routingContext.response();
	        // log("new msg "+routingContext.getBodyAsString());
	        JsonObject res = routingContext.getBodyAsJson();
	        if (res == null) {
	            sendError(400, response);
	        } else {
	            String attribute = res.getString("attribute");
	            String minValue = res.getString("minValue");
	            String maxValue = res.getString("maxValue");
	            String id = res.getString("id");
	            long time = System.currentTimeMillis();
	            OptimalValue optimalValue = new OptimalValue(attribute, minValue, maxValue);
	            if (this.optimalValues.containsKey(Integer.parseInt(id))) {
	                this.optimalValues.get(Integer.parseInt(id)).addFirst(optimalValue);
	            } else {
	                LinkedList<OptimalValue> list = new LinkedList<>();
	                list.add(optimalValue);
	                this.optimalValues.put(Integer.parseInt(id), list);
	            }
	            if (values.size() > MAX_SIZE) {
	                values.get(Integer.parseInt(id)).removeLast();
	            }
	            
	            log("attribute: " + attribute + " minValue: " + minValue + " maxValue: " + maxValue + " on " + new Date(time));
	            response.setStatusCode(200).end();
	        }
	    }
	    
	    private void handleGetOptimalValue(RoutingContext routingContext) {
	        HttpServerRequest request = routingContext.request();
	        MultiMap params =  request.params();
	        String id = params.get("id");
	        JsonArray arr = new JsonArray();
	        if (this.optimalValues.get(Integer.parseInt(id)) != null) {
    	        for (OptimalValue p: this.optimalValues.get(Integer.parseInt(id))) {
    	            JsonObject data = new JsonObject();
    	            data.put("attribute", p.getAttribute());
    
    	            data.put("minValue", p.getMinValue());
    	            data.put("maxValue", p.getMaxValue());
    	            arr.add(data);
    	            //this.optimalValues.get(Integer.parseInt(id)).remove(p);
    	        }
    	        this.optimalValues.get(Integer.parseInt(id)).clear();
	        }
	        routingContext.response()
	            .putHeader("content-type", "application/json")
	            .end(arr.encodePrettily());
	    }
	
	private void sendError(int statusCode, HttpServerResponse response) {
		response.setStatusCode(statusCode).end();
	}

	private void log(String msg) {
		System.out.println("[DATA SERVICE] "+msg);
	}
}