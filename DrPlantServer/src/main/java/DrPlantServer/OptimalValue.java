package DrPlantServer;

public class OptimalValue {

    private final String attribute;
    private final String minValue;
    private final String maxValue;
    
    public OptimalValue(String attribute, String minValue, String maxValue) {
        this.attribute = attribute;
        this.minValue = minValue;
        this.maxValue = maxValue;
    }
    
    public final String getAttribute() {
        return attribute;
    }
    public final String getMinValue() {
        return minValue;
    }
    public final String getMaxValue() {
        return maxValue;
    }
    
    
}
