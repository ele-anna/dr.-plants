#include "AirQualitySensor.h"

AirQualitySensor::AirQualitySensor(int pin) {
    this->pin = pin;
    pinMode(this->pin, INPUT);
}

int AirQualitySensor::getValue(){
    return analogRead(pin);
}