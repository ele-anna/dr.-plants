#ifndef __AIRQUALITY__
#define __AIRQUALITY__

#include "Arduino.h"

class AirQualitySensor
{
public:
    AirQualitySensor(int pin);
    int getValue();

private:
    int pin;
};

#endif