#include "AirQualityTask.h"

AirQualityTask::AirQualityTask(AirQualitySensor *airqual, AlarmTask *alarmTask, Sender *sender)
{
    this->airqual = airqual;
    this->currentValue = 0;
    this->alarmTask = alarmTask;
    this->active = false;
    this->sender = sender;
}

void AirQualityTask::init(int period)
{
    Task::init(period);
    state = TRAKING;
}

void AirQualityTask::setActive(bool active)
{
    this->active = active;
}

bool AirQualityTask::isActive()
{
    return this->active;
}

void AirQualityTask::deactivateAlarmTask()
{
    //potrebbe essersi disattivato da solo a seguito della conferma dell'utente
    if (alarmTask->isActive())
    {
        alarmTask->setActive(false);
    }
    this->sender->notifyNormalState(TAG);
}

void AirQualityTask::determinateSlot()
{
    if(currentValue <=  MAX_GOOD)
    {
        //questi tipi di messaggi hanno data come un array di stringhe con data[0]=Good e data[1]=value
        // this->sender->notifyData(TAG, "[Good," + String(currentValue) + "]");
        if (state == TRAKING_IN_ALARM)
        {
            this->sender->notifyNormalState(TAG);
            this->deactivateAlarmTask();
            state = TRAKING;
        }
    }
    else if (currentValue <= MAX_MODERATE && currentValue > MAX_GOOD)
    {
        // this->sender->notifyData(TAG, "[Moderate," + String(currentValue) + "]");
        if (state == TRAKING_IN_ALARM)
        {
            this->sender->notifyNormalState(TAG);
            this->deactivateAlarmTask();
            state = TRAKING;
        }
    }
    else if (currentValue <= MAX_UNHEALTY_SENS && currentValue > MAX_MODERATE)
    {
        // this->sender->notifyData(TAG, "[Unhealthy for sensitive group," + String(currentValue) + "]");
        if (state == TRAKING_IN_ALARM)
        {
            this->sender->notifyNormalState(TAG);
            this->deactivateAlarmTask();
            state = TRAKING;
        }
    }
    else if (currentValue > MAX_UNHEALTY_SENS)
    {
        // this->sender->notifyData(TAG, "[Unhealty," + String(currentValue) + "]");
        if (state != TRAKING_IN_ALARM)//torniamo nello stato di allarme solo se 
        {   
            this->sender->notifyAlarmState(TAG, "high");                                                  // l'utente non ha già preso visione di questo e non ci siamo già
            state = ALARM;
        }
    }
}

void AirQualityTask::update()
{
    int valueTracked = airqual->getValue();
    if (valueTracked != currentValue)
    {
        currentValue = valueTracked;
        this->determinateSlot();
    }
    this->sender->notifyData(TAG, String(currentValue));
}

void AirQualityTask::tick()
{
                Serial.println("AIR_QUA TICK");
    switch (state)
    {
    case TRAKING:
    {
        update();
        break;
    }
    case ALARM:
    {
        alarmTask->setActive(true);
        state = TRAKING_IN_ALARM;
        break;
    }
    case TRAKING_IN_ALARM:
    {
        update();
        break;
    }
    }
}