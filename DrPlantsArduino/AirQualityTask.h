#ifndef __AIRQUALTASK__
#define __AIRQUALTASK__

#define MAX_GOOD 50
#define MAX_MODERATE 100
#define MAX_UNHEALTY_SENS 150
#define MAX_UNHEALTY 200
#define MAX_VERY_UNH 300
#define TAG "AIR_QUALITY"

#include "Task.h"
#include "AirQualitySensor.h"
#include "AlarmTask.h"
#include "Sender.h"

class AirQualityTask : public Task
{
    AirQualitySensor *airqual;
    bool active;
    int currentValue;
    AlarmTask *alarmTask;
    Sender *sender;

    enum
    State{
        TRAKING,
        ALARM,
        TRAKING_IN_ALARM  
    } state;

public:
    AirQualityTask(AirQualitySensor *airqual,  AlarmTask *alarmTask, Sender *sender);
    void init(int period);
    void tick();
    void setActive(bool active);
    bool isActive();

private:
    void determinateSlot();
    void update();
    void deactivateAlarmTask();
};

#endif