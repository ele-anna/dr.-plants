#include "AlarmTask.h"

AlarmTask::AlarmTask(BlinkingTask *blinking)
{
    this->blinking = blinking;
    this->active = false;
}

void AlarmTask::init(int period)
{
    Task::init(period);
    state = BLINKING;
}

void AlarmTask::setActive(bool active)
{
    this->active = active;
    if (!this->active)
    {
        blinking->setActive(false);
        state = BLINKING;
    }
}

bool AlarmTask::isActive()
{
    return this->active;
}

void AlarmTask::setColor(int red, int green, int blue)
{
    this->red = red;
    this->green = green;
    this->blue = blue;
}

void AlarmTask::tick()
{
    switch (state)
    {
        case BLINKING:
        {
            blinking->setColor(red, green, blue);
            blinking->setActive(true);
            state = RUNNING;
            break;
        }
        case RUNNING:
        {
            //ascolto conferma utente
            break;
        }
        case END:
        {
            blinking->setActive(false);
            this->setActive(false);
            state = BLINKING;
            break;
        }
    }
}