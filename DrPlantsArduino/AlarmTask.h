#ifndef __ALARMTASK__
#define __ALARMTASK__

#include "Task.h"
#include "BlinkingTask.h"

class AlarmTask: public Task {
    BlinkingTask *blinking;
    bool active;
    int red;
    int green;
    int blue;

    enum
    {
        BLINKING,
        RUNNING,
        END
    } state;

public:
    AlarmTask( BlinkingTask *blinking);
    void setActive(bool active);
    bool isActive();
    void init(int period);
    void tick();
    void setColor(int red, int green, int blue);
};
    

#endif
