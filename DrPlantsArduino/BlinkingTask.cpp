#include "BlinkingTask.h"

BlinkingTask::BlinkingTask(LedRGB *led)
{
    this->led = led;
    this->active = false;
}

void BlinkingTask::init(int period)
{
    Task::init(period);
    state = ON;
}

void BlinkingTask::setActive(bool active)
{
    this->active = active;
    if (!this->active)
    {
        led->switchOff();
    }
}

bool BlinkingTask::isActive()
{
    return this->active;
}

void BlinkingTask::setColor(int red, int green, int blue)
{
    this->red = red;
    this->green = green;
    this->blue = blue;
}

void BlinkingTask::tick()
{
    switch (state)
    {
        case ON:
        {
            led->switchOn(red, green, blue);
            state = OFF;
            break;
        }
        case OFF:
        {
            led->switchOff();
            state = ON;
            break;
        }
    }
}
