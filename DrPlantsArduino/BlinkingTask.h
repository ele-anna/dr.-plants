#ifndef __BLINKINGTASK__
#define __BLINKINGTASK__

#include "Task.h"
#include "LedRGB.h"

class BlinkingTask: public Task {
    LedRGB *led;
    bool active;
    int red;
    int green;
    int blue;

    enum
    {
        ON,
        OFF
    } state;

public:
    BlinkingTask( LedRGB *led);
    void setActive(bool active);
    bool isActive();
    void init(int period);
    void tick();
    void setColor(int red, int green, int blue);
};
    

#endif
