#ifndef __BRIGHTNESS__
#define __BRIGHTNESS__

class Brightness
{
public:
    virtual float getBrightness();
};

#endif