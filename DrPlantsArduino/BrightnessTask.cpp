#include "BrightnessTask.h"

BrightnessTask::BrightnessTask(Brightness *brightness, AlarmTask *alarmTask, Sender *sender)
{
    this->brightness = brightness;
    this->alarmTask = alarmTask;
    this->active = false;
    this->sender = sender;
    this->minBrightness = -1.00;
    this->maxBrightness = -1.00;
    this->currentBrightness = -1.00;
}

float BrightnessTask::getMinBrightness()
{
    return this->minBrightness;
}

void BrightnessTask::setMinBrightness(float minBrightness)
{
    // Serial.println("Sono brightness e setto min value");
    this->minBrightness = minBrightness;
}

float BrightnessTask::getMaxBrightness()
{
    return this->maxBrightness;
}

void BrightnessTask::setMaxBrightness(float maxBrightness)
{
    // Serial.println("Sono brightness e setto max value");
    this->maxBrightness = maxBrightness;
}

void BrightnessTask::init(int period, CorrectionTask *correctionTask)
{
    Task::init(period);
    this->correctionTask = correctionTask;
    state = START;
}

void BrightnessTask::setActive(bool active)
{
    this->active = active;
}

bool BrightnessTask::isActive()
{
    return this->active;
}

bool BrightnessTask::isBrightnessGood()
{
    float brightness = this->brightness->getBrightness();
    if (brightness != this->currentBrightness)
    {
        this->currentBrightness = brightness;
    }
    this->sender->notifyData(TAG, String(brightness));
    return (this->currentBrightness<this->minBrightness && this->currentBrightness> this->maxBrightness);
}

bool BrightnessTask::isBrightnessGreaterThanMax()
{
    return this->currentBrightness > this->maxBrightness;
}

void BrightnessTask::tick()
{
                Serial.println("BRIGHTNESS TICK");
    switch (state)
    {
    case START:
    {
        if ((this->minBrightness != -1.00) && (this->maxBrightness != -1.00))
        {
            // Serial.println("Sono brightness, stato=tracking");
            state = TRACKING;
        }
        break;
    }
    case TRACKING:
    {
        if (!this->isBrightnessGood())
        {
            if (correctionTask->isActive() && this->isBrightnessGreaterThanMax())
            {
                correctionTask->switchOffLed();
                correctionTask->setActive(false);
            }
            else
            {
                state = ALARM;
                alarmTask->setActive(true);
            }
        }
        break;
    }
    case ALARM:
    {
        if (this->currentBrightness < this->minBrightness)
        {
            if(!this->correctionTask->isActive()){
                correctionTask->setActive(true);
            }
            this->sender->notifyAlarmState(TAG, "low");
        }
        else if (this->currentBrightness > this->maxBrightness)
        {
            this->sender->notifyAlarmState(TAG, "high");
            alarmTask->setActive(true);
        }
        state = TRACKING_IN_ALARM;

        break;
    }
    case TRACKING_IN_ALARM:
    {
        if (this->isBrightnessGood())
        {
            this->sender->notifyNormalState(TAG);
            if (alarmTask->isActive())
            {
                alarmTask->setActive(false);
            }
            state = TRACKING;
        }
        break;
    }
    }
}