#ifndef __BRIGHTNESS_TASK__
#define __BRIGHTNESS_TASK__

#define TAG "BRIGHTNESS"

#include "Task.h"
#include "Brightness.h"
#include "AlarmTask.h"
#include "CorrectionTask.h"
#include "Sender.h"

class BrightnessTask: public Task {
    Brightness *brightness;
    AlarmTask *alarmTask;
    CorrectionTask *correctionTask;
    Sender *sender;

    bool active;
    float minBrightness;
    float maxBrightness;
    float currentBrightness;

    enum
    {
        START,
        TRACKING,
        ALARM,
        TRACKING_IN_ALARM
    } state;

    public:
    BrightnessTask(Brightness *brightness, AlarmTask *alarmTask, Sender *sender);
    float getMinBrightness();
    void setMinBrightness(float minBrightness);
    float getMaxBrightness();
    void setMaxBrightness(float maxBrightness);
    void init(int period, CorrectionTask *correctionTask);
    void setActive(bool active);
    bool isActive();
    void tick();
    private:
    bool isBrightnessGood();
    bool isBrightnessGreaterThanMax();
};

#endif