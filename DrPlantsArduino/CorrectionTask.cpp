#include "CorrectionTask.h"

CorrectionTask::CorrectionTask(Led *led, AlarmTask *alarmTask, Task *brightnessTask, Brightness *photoresistor)
{ 
    this->led = led;
    this->active = false;
    this->failed = true;
    this->on = false;
    this->alarmTask = alarmTask;
    this->brightnessTask = brightnessTask;
    this->photoresistor = photoresistor;
}

void CorrectionTask::init(int period)
{
    Task::init(period);
    state = START;
}

void CorrectionTask::setActive(bool active)
{
    this->active = active;
}

bool CorrectionTask::isActive()
{
    return this->active;
}

bool CorrectionTask::isLedSwitchOn()
{
    return this->on;
}

void CorrectionTask::switchOffLed()
{
    this->on = false;
    this->led->switchOff();
}

void CorrectionTask::setMaxLight(int maxLight)
{
    this->maxLight = maxLight;
}

void CorrectionTask::setMinLight(int minLight)
{
    this->minLight = minLight;
}

void CorrectionTask::tick()
{
    switch (state)
    {
        case START:
        {
            currentLight = photoresistor->getBrightness();
            led->switchOn();
            on = true;
            state = ADJUSTAMENT;
            break;
        }
        case ADJUSTAMENT:
        {
            float newBrightness = photoresistor->getBrightness();
            if(newBrightness > currentLight && newBrightness > minLight && newBrightness < maxLight)
            {
                failed = false;
            }
            state = END;
            break;
        }
        case END:
        {
            if (failed)
            {
                if(!alarmTask->isActive()){
                    alarmTask->setActive(true);
                }
            }
            break;
        }
    }
}