#ifndef __CORRECTIONTASK__
#define __CORRECTIONTASK__

#include "Task.h"
#include "Led.h"
#include "AlarmTask.h"
#include "Brightness.h"

class CorrectionTask : public Task
{
    Led *led;
    AlarmTask *alarmTask;
    Task *brightnessTask;
    Brightness *photoresistor;
    int minLight;
    int maxLight;
    int currentLight;
    bool active;
    bool failed;
    bool on;
    
    enum
    {
        START,
        ADJUSTAMENT,
        END
    } state;


public:
    CorrectionTask(Led *led, AlarmTask *alarmTask, Task *brightnessTask, Brightness *photoresistor);
    void init(int period);
    void tick();
    void setActive(bool active);
    bool isActive();
    void setMinLight(int minLight);
    void setMaxLight(int maxLight);
    bool isLedSwitchOn();
    void switchOffLed();
};

#endif