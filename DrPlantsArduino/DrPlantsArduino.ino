#include "Scheduler.h"
#include "Light.h"
#include "Led.h"
#include "LedRGB.h"
#include "Brightness.h"
#include "Photoresistor.h"
#include "TemperatureAndHumidity.h"
#include "Environment.h"

#include "BrightnessTask.h"
#include "HumidityTask.h"
#include "AirQualitySensor.h"
#include "BlinkingTask.h"
#include "AlarmTask.h"
#include "AirQualityTask.h"
#include "CorrectionTask.h"
#include "TemperatureTask.h"

#include "MsgServiceEsp.h"
#include "Sender.h"
#include "ListenerTask.h"

#define RX 5
#define TX 6

#define SCHEDULE_TIME 20
#define PIN_LED 13
#define PIN_BLUE 9
#define PIN_GREEN 10
#define PIN_RED 11
#define PIN_AIRQUA A0
#define PIN_FOTORES A1
#define DHT_SENSOR_TYPE DHT_TYPE_11
#define DHT_SENSOR_PIN 3

Led *led;
LedRGB *ledRGB;
Photoresistor *photores;
Environment *temphum;

AlarmTask *alarmAirQualityTask;
AlarmTask *alarmBrightnessTask;
AlarmTask *alarmTempTask;
AlarmTask *alarmHumTask;

AirQualitySensor *airQualityModule;
Scheduler scheduler;
BlinkingTask *blinkingTaskAir;
BlinkingTask *blinkingTaskBrightness;
BlinkingTask *blinkingTaskHumid;
BlinkingTask *blinkingTaskTemp;
AirQualityTask *airQualityTask;
BrightnessTask *brightnessTask;
CorrectionTask *correctionTask;
HumidityTask *humidityTask;
TemperatureTask *temperatureTask;

MsgServiceEsp *msgEsp;
Sender *sender;
ListenerTask *listener;

void setup()
{
  Serial.begin(9600);
  msgEsp = new MsgServiceEsp(RX, TX);
  msgEsp->init();
  sender = new Sender(msgEsp);
  
  scheduler.init(SCHEDULE_TIME);

  led = new Led(PIN_LED);
  ledRGB = new LedRGB(PIN_RED, PIN_GREEN, PIN_BLUE);
  photores = new Photoresistor(PIN_FOTORES);
  temphum = new TemperatureAndHumidity(DHT_SENSOR_PIN);
  airQualityModule = new AirQualitySensor(PIN_AIRQUA);

  blinkingTaskAir = new BlinkingTask(ledRGB);
  blinkingTaskBrightness = new BlinkingTask(ledRGB);
  blinkingTaskHumid = new BlinkingTask(ledRGB);
  blinkingTaskTemp = new BlinkingTask(ledRGB);

  alarmAirQualityTask = new AlarmTask(blinkingTaskAir);
  alarmBrightnessTask = new AlarmTask(blinkingTaskBrightness);
  alarmTempTask = new AlarmTask(blinkingTaskTemp);
  alarmHumTask = new AlarmTask(blinkingTaskHumid);

  airQualityTask = new AirQualityTask(airQualityModule, alarmAirQualityTask, sender);
  brightnessTask = new BrightnessTask(photores, alarmBrightnessTask, sender);
  correctionTask = new CorrectionTask(led, alarmBrightnessTask, brightnessTask, photores);
  humidityTask = new HumidityTask(temphum, alarmHumTask, sender);
  temperatureTask = new TemperatureTask(temphum, alarmTempTask, sender);

  listener = new ListenerTask(brightnessTask, humidityTask, temperatureTask, airQualityTask, msgEsp);
  
  listener->init(20);

  alarmAirQualityTask->init(60);
  alarmBrightnessTask->init(60);
  alarmHumTask->init(60);
  alarmTempTask->init(60);
//DA SISTEMARE !!!!
  blinkingTaskAir->init(1000);
  blinkingTaskBrightness->init(1000);
  blinkingTaskHumid->init(1000);
  blinkingTaskTemp->init(1000);
  airQualityTask->init(10000); // 1 volta ogni 10 min 600 000
  brightnessTask->init(10000, correctionTask);
  correctionTask->init(10000);
  humidityTask->init(10000);
  temperatureTask->init(10000);

  alarmAirQualityTask->setColor(0, 255, 255);
  alarmHumTask->setColor(0, 0, 255);
  alarmBrightnessTask->setColor(255, 255, 0);
  alarmTempTask->setColor(128, 0, 128);

  airQualityTask->setActive(false);
  brightnessTask->setActive(true);
  humidityTask->setActive(true);
  temperatureTask->setActive(true);
  listener->setActive(true);
  scheduler.addTask(listener);


  scheduler.addTask(alarmAirQualityTask);
  scheduler.addTask(alarmBrightnessTask);
  scheduler.addTask(alarmHumTask);
  scheduler.addTask(alarmTempTask);

  scheduler.addTask(airQualityTask);
  scheduler.addTask(blinkingTaskAir);
  scheduler.addTask(brightnessTask);
  scheduler.addTask(blinkingTaskBrightness);
  scheduler.addTask(correctionTask);
  scheduler.addTask(humidityTask);
  scheduler.addTask(blinkingTaskHumid);
  scheduler.addTask(temperatureTask);
  scheduler.addTask(blinkingTaskTemp);
}

void loop()
{
  scheduler.schedule();
}
