#ifndef __ENVIROMENT__
#define __ENVIROMENT__


class Environment
{
public:
    virtual float getTemperature();
    virtual float getHumidity();
};

#endif