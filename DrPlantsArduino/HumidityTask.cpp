#include "HumidityTask.h"

HumidityTask::HumidityTask(Environment *environment, AlarmTask *alarmTask, Sender *sender)
{
    this->environment = environment;
    this->alarmTask = alarmTask;
    this->sender = sender;
    this->active = false;
    this->minHumidity = -1.00;
    this->maxHumidity = -1.00;
    this->currentHumidity= -1.00;
}

float HumidityTask::getMinHumidity()
{
    return this->minHumidity;
}

void HumidityTask::setMinHumidity(float minHumidity)
{
    Serial.println("Sono humidity e setto min value");
    this->minHumidity = minHumidity;
}

float HumidityTask::getMaxHumidity()
{
    return this->maxHumidity;
}

void HumidityTask::setMaxHumidity(float maxHumidity)
{
    Serial.println("Sono humidity e setto max value");
    this->maxHumidity = maxHumidity;
}

void HumidityTask::init(int period)
{
    Task::init(period);
    state = START;
}

void HumidityTask::setActive(bool active)
{
    this->active = active;
}

bool HumidityTask::isActive()
{
    return this->active;
}

bool HumidityTask::isHumidityGood()
{
    humidity = environment->getHumidity();
    Serial.println("HUMIDITY="+String(humidity));
    if (humidity != this->currentHumidity)
    {
        this->currentHumidity = humidity;
    }
    this->sender->notifyData(TAG, String(currentHumidity));
    return (this->currentHumidity < this->minHumidity && this->currentHumidity > this->maxHumidity);
}

void HumidityTask::tick()
{
    Serial.println("HUMIDITY STATE="+String(state));
    switch (state)
    {
    case START:
    {
        if ((this->minHumidity != -1.00) && (this->maxHumidity != -1.00))
        {
            state = TRACKING;
        }
        break;
    }
    case TRACKING:
    {
        if (!this->isHumidityGood())
        {
            state = ALARM;
        }
        break;
    }
    case ALARM:
    {
        alarmTask->setActive(true);


        if (this->currentHumidity < this->minHumidity)
        {
            this->sender->notifyAlarmState(TAG, "low");
        }
        else if (this->currentHumidity > this->maxHumidity)
        {
            this->sender->notifyAlarmState(TAG, "high");
        }
        state = TRACKING_IN_ALARM;

        break;
    }
    case TRACKING_IN_ALARM:
    {
        if (this->isHumidityGood())
        {
            this->sender->notifyNormalState(TAG);
            if(alarmTask->isActive())
            {
                alarmTask->setActive(false);
            }
            state = TRACKING;
        }
        break;
    }
    }
}