#ifndef __HUMIDITY_TASK__
#define __HUMIDITY_TASK__

#define TAG "HUMIDITY"

#include "Task.h"
#include "TemperatureAndHumidity.h"
#include "AlarmTask.h"
#include "Sender.h"

class HumidityTask: public Task {
    Environment *environment;
    AlarmTask *alarmTask;
    Sender *sender;
    bool active;
    float minHumidity;
    float maxHumidity;
    float currentHumidity;
    float humidity;

    enum
    {
        START,
        TRACKING,
        ALARM,
        TRACKING_IN_ALARM
    } state;

    public:
    HumidityTask(Environment *enviroment, AlarmTask *alarmTask, Sender *sender);
    float getMinHumidity();
    void setMinHumidity(float minHumidity);
    float getMaxHumidity();
    void setMaxHumidity(float maxHumidity);
    void init(int period);
    void setActive(bool active);
    bool isActive();
    void tick();
    private:
    bool isHumidityGood();
};

#endif