#include "Led.h"


Led::Led(int pin) {
  this->pin = pin;
  this->on = false;
  pinMode(pin, OUTPUT);
}

void Led::switchOn() {
  on = true;
  digitalWrite(pin, HIGH);
}

void Led::switchOff() {
  on = false;
  digitalWrite(pin, LOW);
};

bool Led::isSwitchedOn() {
  return on;
};