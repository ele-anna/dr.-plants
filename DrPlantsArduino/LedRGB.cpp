#include "LedRGB.h"

LedRGB::LedRGB(int pinRed, int pinGreen, int pinBlue)
{
  this->pinRed = pinRed;
  this->pinGreen = pinGreen;
  this->pinBlue = pinBlue;
  this->on = false;
  pinMode(pinRed, OUTPUT);
  pinMode(pinGreen, OUTPUT);
  pinMode(pinBlue, OUTPUT);
}

void LedRGB::switchOn(int red, int green, int blue)
{
  on = true;
  analogWrite(pinRed, red);
  analogWrite(pinGreen, green);
  analogWrite(pinBlue, blue);
}

void LedRGB::switchOff()
{
  on = false;
  analogWrite(pinRed, 0);
  analogWrite(pinGreen, 0);
  analogWrite(pinBlue, 0);
};

bool LedRGB::isSwitchedOn()
{
  return on;
};