#ifndef __LEDRGB__
#define __LEDRGB__

#include "Light.h"
#include "Arduino.h"

class LedRGB: public Light
{
public:
    LedRGB(int pinRed, int pinGreen, int pinBlue);
    void switchOn(int red, int green, int blue);
    void switchOff();
    bool isSwitchedOn();

private:
    int pinRed;
    int pinBlue;
    int pinGreen;
    bool on;
};

#endif