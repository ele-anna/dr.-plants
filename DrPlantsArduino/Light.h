#ifndef __LIGHT__
#define __LIGHT__

class Light
{
public:
    virtual void switchOff();
    virtual bool isSwitchedOn();
};

#endif