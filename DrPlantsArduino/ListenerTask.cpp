#include "ListenerTask.h"

ListenerTask::ListenerTask(BrightnessTask *brightnessTask, HumidityTask *humidityTask, TemperatureTask *temperatureTask, AirQualityTask* airQualityTask, MsgServiceEsp *msgEsp)
{
    this->brightnessTask = brightnessTask;
    this->humidityTask = humidityTask;
    this->temperatureTask = temperatureTask;
    this->airQualityTask = airQualityTask;
    this->msgEsp = msgEsp;
    this->active = false;
}

void ListenerTask::setHumidityTask()
{
    Serial.println("ListenerTask::setHumidityTask");
    this->humidityTask->setMaxHumidity(minValue.toFloat());
    this->humidityTask->setMinHumidity(maxValue.toFloat());
}

void ListenerTask::setTemperatureTask()
{
    this->temperatureTask->setMaxTemperature(minValue.toFloat());
    this->temperatureTask->setMinTemperature(maxValue.toFloat());
}

void ListenerTask::setBrightnessTask()
{
    this->brightnessTask->setMaxBrightness(minValue.toFloat());
    this->brightnessTask->setMinBrightness(maxValue.toFloat());
    
}

void ListenerTask::init(int period)
{
    Task::init(period);
}

void ListenerTask::setActive(bool active)
{
    this->active = active;
}

bool ListenerTask::isActive()
{
    return this->active;
}

void ListenerTask::tick()
{
    if(msgEsp->isMsgAvailable()) 
    {
        // Serial.println("message available");
        Msg *message = msgEsp->receiveMsg();
        String m = message->getContent();
        int len = m.length();
        char msg[len];
        m.toCharArray(msg, len);

        
        task = strtok(msg, "+");
        m.substring(task.length(), m.length()).toCharArray(msg, len);
        minValue = strtok(msg, "+");
        m.substring(task.length() + minValue.length()+2, m.length()).toCharArray(msg, len);
        maxValue = strtok(msg, "+");

        Serial.println("task " + task);
        //  Serial.println("minValue " + minValue);
        //  Serial.println("maxValue " + maxValue);

        if (task.equals(HUMIDITY_TASK)) {
            setHumidityTask();
        } else if (task.equals(TEMPERATURE_TASK)) {
            setTemperatureTask();
        } else if (task.equals(BRIGHTNESS_TASK)) {
            setBrightnessTask();
        }else if (task.equals(AIR_QUALITY_TASK))
        {
            this->airQualityTask->setActive(true);
        }

        delete message;
    }
}