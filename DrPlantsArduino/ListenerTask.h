#ifndef __LISTENER_TASK__
#define __LISTENER_TASK__

#define HUMIDITY_TASK "HUMIDITY"
#define TEMPERATURE_TASK "TEMPERATURE"
#define BRIGHTNESS_TASK "BRIGHTNESS"
#define AIR_QUALITY_TASK "AIR_QUALITY"

#include "BrightnessTask.h"
#include "HumidityTask.h"
#include "TemperatureTask.h"
#include "AirQualityTask.h"
#include "MsgServiceEsp.h"

class ListenerTask: public Task {
    bool active;
    BrightnessTask *brightnessTask;
    HumidityTask *humidityTask;
    TemperatureTask *temperatureTask;
    AirQualityTask *airQualityTask;
    MsgServiceEsp *msgEsp;

    public:
    ListenerTask(BrightnessTask *brightnessTask, HumidityTask *humidityTask, TemperatureTask *temperatureTask, AirQualityTask* airQualityTask, MsgServiceEsp *msgEsp);
    void init(int period);
    void setActive(bool active);
    bool isActive();
    void tick();
    
    private:
    String task;
    String minValue;
    String maxValue;
    void setHumidityTask();
    void setTemperatureTask();
    void setBrightnessTask();
};

#endif