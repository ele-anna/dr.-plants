#include "Sender.h"

Sender::Sender(MsgServiceEsp *msgEsp)
{
    this->msgEsp = msgEsp;
}

void Sender::notifyData(String tag, String data)
{
    // Serial.println("notifyData->" + Msg("DATA " + tag + " " + data).getContent());
    msgEsp->sendMsg("DATA+" + tag + "+" + data);
}

void Sender::notifyAlarmState(String tag, String message)
{
    // Serial.println("notifyAlarmState->" + Msg("ALARM " + tag + " " + message).getContent());
    msgEsp->sendMsg("STATE+" + tag + "+" + message);
}

void Sender::notifyNormalState(String tag)
{
    // Serial.println("notifyNormalState->" + Msg("NORMAL " + tag).getContent());
    msgEsp->sendMsg("STATE+" + tag + "+NORMAL");
}