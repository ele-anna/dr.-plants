#ifndef __SENDER__
#define __SENDER__

#include "Arduino.h"
#include "MsgServiceEsp.h"

class Sender
{
public:
    Sender(MsgServiceEsp *msgEsp);
    void notifyData(String tag, String data);
    void notifyAlarmState(String tag);
    void notifyAlarmState(String tag, String message);
    void notifyNormalState(String tag);
private:
    MsgServiceEsp *msgEsp;
};

#endif 