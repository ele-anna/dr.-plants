#ifndef __TASK__
#define __TASK__

#include "Arduino.h"
class Task
{

public:
  Task()
  {
  }

  virtual void init(int period)
  {
    myPeriod = period;
    timeElapsed = 0;
  }

  virtual void tick() = 0;

  bool updateAndCheckTime(int basePeriod)
  {
    timeElapsed += basePeriod;
    if (timeElapsed >= myPeriod)
    {
      timeElapsed = 0;
      return true;
    }
    else
    {
      return false;
    }
  }

  virtual bool isActive();

  virtual void setActive(bool active);

private:
  int myPeriod;
  int timeElapsed;
  bool active;
};

#endif
