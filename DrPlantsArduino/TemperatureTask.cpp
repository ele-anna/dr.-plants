#include "TemperatureTask.h"

TemperatureTask::TemperatureTask(Environment *environment, AlarmTask *alarmTask, Sender *sender)
{
    this->environment = environment;
    this->alarmTask = alarmTask;
    this->sender = sender;
    this->active = false;
    this->minTemperature = -100.00;
    this->maxTemperature = -100.00;
    this->currentTemperature = -100.00;
}

float TemperatureTask::getMinTemperature()
{
    return this->minTemperature;
}

void TemperatureTask::setMinTemperature(float minTemperature)
{
    // Serial.println("Sono temperature e ho settato min value");
    this->minTemperature = minTemperature;
}

float TemperatureTask::getMaxTemperature()
{
    return this->maxTemperature;
}

void TemperatureTask::setMaxTemperature(float maxTemperature)
{
    // Serial.println("Sono temperature e ho settato max value");
    this->maxTemperature = maxTemperature;
}

void TemperatureTask::init(int period)
{
    Task::init(period);
    state = START;
}

void TemperatureTask::setActive(bool active)
{
    this->active = active;
}

bool TemperatureTask::isActive()
{
    return this->active;
}

bool TemperatureTask::isTemperatureGood()
{
    temperature = environment->getTemperature();
                Serial.println("TEMPERATURE="+String(temperature));
    if (temperature != this->currentTemperature)
    {
        this->currentTemperature = temperature;
    } 
    this->sender->notifyData(TAG, String(currentTemperature));
    return (this->currentTemperature < this->minTemperature && this->currentTemperature > this->maxTemperature);
}

void TemperatureTask::tick()
{
                Serial.println("TEMPERATURE STATE="+String(state));
    switch (state)
    {
    case START:
    {
        if ((this->minTemperature != -100.00) && (this->maxTemperature != -100.00))
        {
            // Serial.println("Sono TemperatureTask, stato=tracking");
            state = TRACKING;
        }
        break;
    }
    case TRACKING:
    {

        if (!this->isTemperatureGood())
        {
            alarmTask->setActive(true);
            state = ALARM;
        }
        break;
    }
    case ALARM:
    {
        if (this->currentTemperature < this->minTemperature)
        {
            this->sender->notifyAlarmState(TAG, "low");
        }
        else if (this->currentTemperature > this->maxTemperature)
        {
            this->sender->notifyAlarmState(TAG, "high");
        }
        state = TRACKING_IN_ALARM;

        break;
    }
    case TRACKING_IN_ALARM:
    {
        if (this->isTemperatureGood())
        {
            this->sender->notifyNormalState(TAG);
            if(alarmTask->isActive())
            {
                alarmTask->setActive(false);
            }
            state = TRACKING;
        }
        break;
    }
    }
}