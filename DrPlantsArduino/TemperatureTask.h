#ifndef __TEMPERATURE_TASK__
#define __TEMPERATURE_TASK__

#define TAG "TEMPERATURE"

#include "Task.h"
#include "Environment.h"
#include "AlarmTask.h"
#include "Sender.h"

class TemperatureTask: public Task {
    Environment *environment;
    AlarmTask *alarmTask;
    Sender *sender;
    bool active;
    float minTemperature;
    float maxTemperature;
    float currentTemperature;
    float temperature;

    enum
    {
        START,
        TRACKING,
        ALARM,
        TRACKING_IN_ALARM
    } state;

    public:
    TemperatureTask(Environment *enviroment, AlarmTask *alarmTask, Sender *sender);
    float getMinTemperature();
    void setMinTemperature(float minTemperature);
    float getMaxTemperature();
    void setMaxTemperature(float maxTemperature);
    void init(int period);
    void setActive(bool active);
    bool isActive();
    void tick();
    private:
    bool isTemperatureGood();
};

#endif