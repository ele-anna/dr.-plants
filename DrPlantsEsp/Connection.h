#ifndef __CONNECTION__
#define __CONNECTION__
#include "Arduino.h"
class Connection {
public:
  virtual void connecting();
  virtual int sendData(String address, String type, String task, String value, int id);
  virtual String getData(String address, String path);
  virtual bool isConnected();    
};

#endif
