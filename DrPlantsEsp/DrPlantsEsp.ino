#include "Connection.h"
#include "MsgServiceSerial.h"
#include "MsgServiceArduino.h"
#include "Esp8266.h"
#include <ArduinoJson.h>
#include <stdio.h>
#include <string.h>

/* wifi network name */
#define SSIDNAME "here the name of the net"
/* WPA2 PSK password */
#define PWD "here the password of the net"
/* service IP address */
#define ADDRESS "here the address of the server"

#define INIT 0
#define RECEIVE 1
#define SEND 2

#define RX D6
#define TX D5

#define FREQ1 5000
#define FREQ2 20

MsgServiceArduino *msgARD;
Connection *conn;

int state;
int id;
int i = 0;
unsigned long previousMillis1 = 0;
unsigned long previousMillis2 = 0;

void setup()
{
    Serial.begin(9600);

    msgARD = new MsgServiceArduino(RX, TX);
    msgARD->init();

    conn = new Esp8266(SSIDNAME, PWD);
    conn->connecting();

    state = INIT;
}

void loop()
{
    switch (state)
    {
    case INIT:
    {
        String msg = conn->getData(ADDRESS, "/api/data/id");
        StaticJsonDocument<20000> data;
        DeserializationError error = deserializeJson(data, msg);
        if (error)
        {
            Serial.print(F("deserializeJson() failed: "));
            Serial.println(error.f_str());
            break;
        }
        id = data["id"].as<int>();
        state = RECEIVE;
        break;
    }
    case RECEIVE:
    {
        unsigned long currentMillis1 = millis();
        if (currentMillis1 - previousMillis1 >= FREQ1)
        {
            previousMillis1 = currentMillis1;

            String msg = conn->getData(ADDRESS, "/api/data/optimalValue/?id=" + String(id));

            if (msg != "[]")
            {
                StaticJsonDocument<20000> data;
                DeserializationError error = deserializeJson(data, msg);
                if (error)
                {
                    Serial.print(F("deserializeJson() failed: "));
                    Serial.println(error.f_str());
                    break;
                }
                for (int i = 0; i < data.size(); i++)
                {
                    msgARD->sendMsg(Msg(data[i]["attribute"].as<String>() + "+" + data[i]["minValue"].as<String>() + "+" + data[i]["maxValue"].as<String>()));
                }
            }
        }
        state = SEND;
        break;
    }
    case SEND:
    {
        unsigned long currentMillis2 = millis();
        if (currentMillis2 - previousMillis2 >= FREQ2)
        {
            previousMillis2 = currentMillis2;

            if (msgARD->isMsgAvailable())
            {
                Msg *message = msgARD->receiveMsg();
                String m = message->getContent();
                int index1 = m.indexOf('+');
                String type = m.substring(0, index1);
                int index2 = m.indexOf('+', index1 + 1);
                String task = m.substring(index1 + 1, index2);
                String value = m.substring(index2 + 1, m.length() - 1);

                //in tutti i casi manda type, task e value. Value può essere vuoto.
                conn->sendData(ADDRESS, type, task, value, id);
                delete message;
            }
        }
        state = RECEIVE;
        break;
    }
    }
}
