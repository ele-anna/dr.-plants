#include "Esp8266.h"
#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>


Esp8266::Esp8266(char *ssidName, char *pwd)
{
    this->ssidName = ssidName;
    this->pwd = pwd;
}

void Esp8266::connecting()
{
    WiFi.begin(ssidName, pwd);
    Serial.print("Connecting...");
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }
    WiFi.setSleepMode(WIFI_NONE_SLEEP);
    Serial.println("Connected: \n local IP: " + WiFi.localIP());
}

int Esp8266::sendData(String address, String type, String task, String value, int id)
{
    if (WiFi.status() != WL_CONNECTED)
    {
        Serial.println("Not connecting");
        connecting();
    }
    if (WiFi.status() == WL_CONNECTED)
    {
        HTTPClient http;
        http.begin(address + "/api/data/");
        http.addHeader("Content-Type", "application/json");

        String msg = String("{ \"type\": \"") + type + String("\", \"task\": \"") + task + 
                     String("\", \"value\": \"") + value + String("\", \"id\": \"") + String(id) + String("\" }");
        Serial.println(msg);
        
        int retCode = http.POST(msg);
        http.end();

        return retCode;
    }
}

String Esp8266::getData(String address, String path) 
{
    if (WiFi.status() != WL_CONNECTED)
    {
        Serial.println("Not connecting");
        connecting();
    }
    if (WiFi.status() == WL_CONNECTED)
    {
        HTTPClient http;
        http.begin(address + path);

        int httpCode = http.GET();
        String payload = http.getString();

        http.end();
        return payload;
    }
}

bool Esp8266::isConnected()
{
    return WiFi.status() == WL_CONNECTED;
}

