#ifndef __ESP__
#define __ESP__

#include "Arduino.h"
#include "Connection.h"

class Esp8266: public Connection { 

    public:
        Esp8266(char *ssidName, char *pwd);
        void connecting();
        int sendData(String address, String type, String task, String value, int id);
        String getData(String address, String path);
        bool isConnected(); 
    private:
        char* ssidName;
        char* pwd;  
};

#endif
