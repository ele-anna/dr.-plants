package com.example.drplantsmobileapp;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.drplantsmobileapp.RecyclerView.AddPlantRecyclerView.OptimalAttributeAdapter;
import com.example.drplantsmobileapp.RecyclerView.AddPlantRecyclerView.OptimalAttributeValue;
import com.example.drplantsmobileapp.RecyclerView.PlantattributeRecyclerView.AttributeItem;
import com.example.drplantsmobileapp.RecyclerView.PlantsRecyclerView.PlantsCardItem;
import com.example.drplantsmobileapp.ViewModel.AddPlantViewModel;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static com.example.drplantsmobileapp.Utilities.REQUEST_IMAGE_CAPTURE;

public class AddPlantFragment extends Fragment {

    private static final String TAG = AddPlantFragment.class.getSimpleName();

    private TextView plantName;
    private ImageView plantImage;
    private TextView plantDescription;
    private ActivityResultLauncher<String> requestPermissionLauncher;
    private AddPlantViewModel addPlantViewModel;
    private OptimalAttributeAdapter optimalAttributeAdapter;
    private RequestQueue requestQueue;
    private final static String OSM_REQUEST_TAG = "OSM_REQUEST";
    private String imageUriString = "ic_launcher_foreground";
    private View addPlantLayout;
    private View failedLayout;
    private View currentLayout;
    private List<OptimalAttributeValue> optimalAttributeValues;

    //callback that keep monitored the internet connection
    private ConnectivityManager.NetworkCallback networkCallback;
    private Snackbar snackbar;
    private Boolean isNetworkConnected = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.add_plant_layout, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getActivity() != null) {
            registerNetworkCallback(getActivity());
            this.saveImageAndCheckPermission(getActivity());
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Activity activity = getActivity();
        if (activity != null) {
            this.snackbar = Snackbar.make(activity.findViewById(R.id.fragment_container_view),
                    "No Internet Available",
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.add_new_plant, v -> {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_WIRELESS_SETTINGS);
                        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                        if (intent.resolveActivity(activity.getPackageManager()) != null) {
                            activity.startActivity(intent);
                        }
                    });

            this.networkCallback = new ConnectivityManager.NetworkCallback(){
                @Override
                public void onAvailable(@NonNull Network network) {
                    super.onAvailable(network);
                    isNetworkConnected = true;
                    Log.d(TAG, "onAvailable");
                    snackbar.dismiss();
                }

                @Override
                public void onLost(@NonNull Network network) {
                    super.onLost(network);
                    isNetworkConnected = false;
                    Log.d(TAG, "on lost");
                    snackbar.show();
                }
            };

            requestQueue = Volley.newRequestQueue(activity);

            this.requestPermissionLauncher = registerForActivityResult(new ActivityResultContracts.RequestPermission(), result -> {
                if (result) {
                    Log.d(TAG, "Permission granted");
                    final Bitmap bitmap = addPlantViewModel.getImageBitmap().getValue();
                    try {
                        this.imageUriString = String.valueOf(saveImage(bitmap, activity));
                        if (isNetworkConnected) {
                            this.sendVolleyRequestPlant(activity);
                        } else {
                            snackbar.show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.d(TAG, "Permission not granted");
                    showDialog(activity);
                }
            });
            Utilities.setUpToolbar((AppCompatActivity) activity, getString(R.string.add_new_plant));
            setRecyclerView(activity);

            this.plantImage = view.findViewById(R.id.add_plant_image);
            this.plantName = view.findViewById(R.id.add_plant_name);
            this.plantDescription = view.findViewById(R.id.add_plant_description);
            View progressLayout = view.findViewById(R.id.progress_layout);
            this.addPlantLayout = view.findViewById(R.id.add_plant_layout);
            this.failedLayout = view.findViewById(R.id.search_fail_layout);
            this.failedLayout.setVisibility(View.GONE);
            this.addPlantLayout.setVisibility(View.GONE);

            view.findViewById(R.id.retry).setOnClickListener(v->{
                final Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
                    activity.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            });

            this.setCurrentLayoutVisible(progressLayout);
            this.addPlantViewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(AddPlantViewModel.class);
            this.addPlantViewModel.getImageBitmap().observe(getViewLifecycleOwner(), bitmap -> plantImage.setImageBitmap(bitmap));
            view.findViewById(R.id.add_plant_button).setOnClickListener(l -> addPlantClicked(activity));
            this.saveImageAndCheckPermission(activity);
        } else {
            Log.d(TAG, "Error, activity is null");
        }
    }

    private void setCurrentLayoutVisible(View layout) {
        if (this.currentLayout != null) {
            this.currentLayout.setVisibility(View.GONE);
        }
        this.currentLayout = layout;
        this.currentLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.app_bar_plants).setVisible(true);
    }

    private String base64EncodeFromFile(Activity activity, Uri fileString) throws Exception {
        ContentResolver resolver = activity.getApplicationContext()
                .getContentResolver();
        InputStream fis = resolver.openInputStream(fileString);

        try {
            int next;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            while((next = fis.read()) != -1) {
                out.write(next);
            }
            byte[] data = out.toByteArray();
            String res = android.util.Base64.encodeToString(data, android.util.Base64.DEFAULT);
            fis.close();
            return res;

        } catch(IOException e) {
            e.printStackTrace();
            Log.d(TAG, "image is null");
            return  null;
        }
    }

    private void sendVolleyRequestPlant(Activity activity){
        String apiKey = "kYUxaShuyW7bFQ3aSYVTDbFmgPINaXvRSn2fQDzHi9KV2FMfnp";

        JSONObject data = new JSONObject();
        try {
            data.put("api_key", apiKey);

            JSONArray modifiers = new JSONArray()
                    .put("crops_fast")
                    .put("similar_images");
            data.put("modifiers", modifiers);
            data.put("plant_language", Locale.getDefault().getLanguage());
            JSONArray plantDetails = new JSONArray()
                    .put("common_names")
                    .put("url")
                    .put("name_authority")
                    .put("wiki_description")
                    .put("taxonomy")
                    .put("synonyms");
            data.put("plant_details", plantDetails);

            Uri imageUri = Uri.parse(this.imageUriString);
            String fileData = this.base64EncodeFromFile(activity, imageUri);
            JSONArray images = new JSONArray();
            images.put(fileData);
            data.put("images", images);

        } catch (Exception e) {
            e.printStackTrace();
        }

        String url = "https://api.plant.id/v2/identify";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, data,
                response -> {
                    try {
                        String plantNameText = response.getJSONArray("suggestions").getJSONObject(0).get("plant_name").toString();
                        String descriptionText = response.getJSONArray("suggestions").getJSONObject(0).getJSONObject("plant_details").getJSONObject("wiki_description").get("value").toString();
                        plantName.setText(plantNameText);
                        plantDescription.setText(descriptionText);
                        this.sendVolleyRequestToken();
                        Log.e(TAG, response.toString());
                    } catch (JSONException e) {
                        this.setCurrentLayoutVisible(this.failedLayout);
                        Log.d(TAG, e.toString());
                    }
                }, error -> {
            this.setCurrentLayoutVisible(this.failedLayout);
            Log.d(TAG, error.toString());
        });

        jsonObjectRequest.setTag(OSM_REQUEST_TAG);
        // Add the request to the RequestQueue.
        requestQueue.add(jsonObjectRequest);

    }

    private void sendVolleyRequestToken(){
        String clientId = "NZmbRFS7eniHCnSwmvjOzhXicA5DPl7EJyVGwpmn";
        String clientSecret = "Fl4rxufoi1v68gPYi1hEDToC5vmMlt5N92lCPSVEtS9n3zqucFu20upiR9JvYqYNCWyKMltf5bwoJf4njfFKrTJ1ktKUUcCpMiS17AVCp1SNsHz6WBz65Ug2zpMOQcjv";
        String url ="https://open.plantbook.io/api/v1/token/";

        StringRequest request = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        sendVolleyRequestPid(new JSONObject(response).get("access_token").toString());
                    } catch (JSONException e) {
                        this.setCurrentLayoutVisible(this.failedLayout);
                        e.printStackTrace();
                    }
                },
                error -> {
                    Log.e(TAG, error.networkResponse.toString());
                    this.setCurrentLayoutVisible(this.failedLayout);
                }
        ) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String,String> params = new HashMap<>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                //..add other headers
                return params;
            }

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<>();
                params.put("grant_type", "client_credentials");
                params.put("client_id", clientId);
                params.put("client_secret", clientSecret);

                return params;
            }

        };

        //requestQueue.setTag(OSM_REQUEST_TAG);
        requestQueue.add(request);
    }

    private void sendVolleyRequestPid(String accessToken) {
        String url ="https://open.plantbook.io/api/v1/plant/search?alias=" + this.plantName.getText().toString() + "&limit=1" ;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                response -> {
                    try {
                        String pid = response.getJSONArray("results").getJSONObject(0).get("pid").toString();
                        sendVolleyRequestAttribute(pid, accessToken);
                    } catch (JSONException e) {
                        this.setCurrentLayoutVisible(this.failedLayout);
                        e.printStackTrace();
                    }
                }, error -> {
            Log.d(TAG, error.toString());
            this.setCurrentLayoutVisible(this.failedLayout);
        }){
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+ accessToken);
                return headers;
            }
        };

        jsonObjectRequest.setTag(OSM_REQUEST_TAG);
        requestQueue.add(jsonObjectRequest);
    }

    private void sendVolleyRequestAttribute(String pid, String accessToken) {
        String url ="https://open.plantbook.io/api/v1/plant/detail/"+ pid +"/" ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url,
                null, response -> {
                    try {
                        Float maxLight = Float.parseFloat(response.get("max_light_lux").toString());
                        Float minLight = Float.parseFloat(response.get("min_light_lux").toString());
                        Float maxTemp = Float.parseFloat(response.get("max_temp").toString());
                        Float minTemp = Float.parseFloat(response.get("min_temp").toString());
                        Float maxHumid = Float.parseFloat(response.get("max_env_humid").toString());
                        Float minHumid = Float.parseFloat(response.get("min_env_humid").toString());

                        if (this.optimalAttributeValues == null) {
                            this.optimalAttributeValues = new LinkedList<>();
                        } else {
                            this.optimalAttributeValues.clear();
                        }
                        this.optimalAttributeValues.add(new OptimalAttributeValue(AttributeItem.BRIGHTNESS.name(), minLight, maxLight, "Lux", null));
                        this.optimalAttributeValues.add(new OptimalAttributeValue(AttributeItem.TEMPERATURE.name(), minTemp, maxTemp, "°", null));
                        this.optimalAttributeValues.add(new OptimalAttributeValue(AttributeItem.HUMIDITY.name(), minHumid, maxHumid, "%", null));
                        this.optimalAttributeValues.add(new OptimalAttributeValue(AttributeItem.AIR_QUALITY.name(), 0, 300, "PPM", null));
                        this.optimalAttributeAdapter.setData(this.optimalAttributeValues);

                        this.setCurrentLayoutVisible(this.addPlantLayout);
                        unRegisterNetworkCallback();
                    } catch (JSONException e) {
                        this.setCurrentLayoutVisible(this.failedLayout);
                        e.printStackTrace();
                    }
                }, error -> Log.d(TAG, error.toString())){
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+ accessToken);
                return headers;
            }
        };

        jsonObjectRequest.setTag(OSM_REQUEST_TAG);
        requestQueue.add(jsonObjectRequest);
    }

    private void registerNetworkCallback(Activity activity) {
        Log.d(TAG,"registerNetworkCallback");
        ConnectivityManager connectivityManager =
                (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                connectivityManager.registerDefaultNetworkCallback(networkCallback);
            } else {
                //Class deprecated since API 29 (android 10) but used for android 6
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                isNetworkConnected = networkInfo != null && networkInfo.isConnected();
            }
        } else {
            isNetworkConnected = false;
        }
    }

    private void unRegisterNetworkCallback() {
        if (getActivity() != null) {
            Log.d(TAG, "unRegisterNetworkCallback");
            ConnectivityManager connectivityManager =
                    (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivityManager != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                   connectivityManager.unregisterNetworkCallback(networkCallback);
                } else {
                    snackbar.dismiss();
                }
            }
        }
    }

    private void setRecyclerView(Activity activity) {
        final RecyclerView recyclerView = requireView().findViewById(R.id.add_plant_recycler_view);
        recyclerView.setNestedScrollingEnabled(false);
        this.optimalAttributeAdapter = new OptimalAttributeAdapter(activity);
        recyclerView.setAdapter(this.optimalAttributeAdapter);
    }

    private void saveImageAndCheckPermission(Activity activity) {
        final Bitmap bitmap = addPlantViewModel.getImageBitmap().getValue();
        if (bitmap != null) {
            //permission
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                final String PERMISSION_REQUESTED = Manifest.permission.WRITE_EXTERNAL_STORAGE;
                if (ActivityCompat.checkSelfPermission(activity, PERMISSION_REQUESTED) == PackageManager.PERMISSION_GRANTED) {
                    try {
                        this.imageUriString = String.valueOf(saveImage(bitmap, activity));
                        if (isNetworkConnected) {

                            this.sendVolleyRequestPlant(activity);
                        } else {
                            snackbar.show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (ActivityCompat.shouldShowRequestPermissionRationale(activity, PERMISSION_REQUESTED)) {
                    showDialog(activity);
                } else {
                    requestPermissionLauncher.launch(PERMISSION_REQUESTED);
                }
            } else {
                try {
                    this.imageUriString = String.valueOf(saveImage(bitmap, activity));
                    if (isNetworkConnected) {
                        this.sendVolleyRequestPlant(activity);
                    } else {
                        snackbar.show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void addPlantClicked(Activity activity) {
        addPlantViewModel.addPlantItem(new PlantsCardItem(this.plantName.getText().toString(), imageUriString, this.plantDescription.getText().toString()), this.optimalAttributeValues);
        addPlantViewModel.setImageBitmap(null);
        this.optimalAttributeValues.clear();
        Utilities.insertFragemnt((AppCompatActivity) activity, new PlantFragment(), PlantFragment.class.getSimpleName());
    }


    private Uri saveImage(Bitmap bitmap, Activity activity) throws IOException {
        // Create an image file name
        final String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ITALY).format(new Date());
        final String name = "JPEG_" + timeStamp;

        final ContentResolver resolver = activity.getContentResolver();
        final Uri contentUri;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            contentUri = MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY);
        } else {
            contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        }
        final ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.Images.Media.DISPLAY_NAME, name + ".jpg");
        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg");
        final Uri imageUri = resolver.insert(contentUri, contentValues);

        final OutputStream fos = resolver.openOutputStream(imageUri);

        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        if (fos != null) {
            fos.close();
        }
        return imageUri;
    }

    private void showDialog(Activity activity) {
        new AlertDialog.Builder(activity)
                .setMessage("Permission was denied, but is needed for the save image functionality.")
                .setCancelable(false) //Sets whether this dialog is cancelable with the BACK key.
                .setPositiveButton("OK", (dialog, id) -> activity.startActivity(
                        new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS)))
                .setNegativeButton("Cancel", (dialog, id) -> dialog.cancel())
                .create()
                .show();
    }
}
