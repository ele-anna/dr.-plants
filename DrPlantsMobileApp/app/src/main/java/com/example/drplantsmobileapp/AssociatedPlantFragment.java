package com.example.drplantsmobileapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import com.example.drplantsmobileapp.DataBase.PlantAndDevice;
import com.example.drplantsmobileapp.RecyclerView.DeviceRecyclerView.Device;
import com.example.drplantsmobileapp.RecyclerView.PlantattributeRecyclerView.AttributeItem;
import com.example.drplantsmobileapp.ViewModel.DeviceViewModel;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;


public class AssociatedPlantFragment extends Fragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.plant_associated, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Activity activity = getActivity();
        if (activity != null) {

            TextInputEditText deviceName = view.findViewById(R.id.text_field_device);
            deviceName.setEnabled(false);


            DeviceViewModel viewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(DeviceViewModel.class);
            Device currentDevice =  viewModel.getSelected().getValue();

            TextView textCard = view.findViewById(R.id.text_card);
            TextView textDate = view.findViewById(R.id.text_dotation_date);
            ImageView imageCard = view.findViewById(R.id.image_card);

            TextView lastBrightness = view.findViewById(R.id.last_brightness);
            TextView lastHumidity = view.findViewById(R.id.last_humidity);
            TextView lastTemperature = view.findViewById(R.id.last_temperature);
            View lastBrightnessIcon = view.findViewById(R.id.sun_layout);
            View lastHumidityIcon = view.findViewById(R.id.humidity_layout);
            View lastTemperatureIcon = view.findViewById(R.id.temperature_layout);

            viewModel.getPlantAssociated(currentDevice.getId()).observe((LifecycleOwner) activity, (PlantAndDevice plantsAndDevice)-> {
                textCard.setText(plantsAndDevice.plant.getPlantName());
                textDate.setText(plantsAndDevice.plant.getDate_association());

                final String image_path = plantsAndDevice.plant.getImageResource();

                if (image_path.contains("ic_")) {
                    final Drawable drawable = ContextCompat.getDrawable(activity, activity.getResources().getIdentifier(image_path, "drawable", activity.getPackageName()));
                    imageCard.setImageDrawable(drawable);
                } else {
                    final Bitmap bitmap = Utilities.getImageBitmap(activity, Uri.parse(image_path));
                    if (bitmap != null) {
                        imageCard.setImageBitmap(bitmap);
                    }
                }

                final View detectionLayout = view.findViewById(R.id.last_detection_layout);
                viewModel.getDetectionsByPlantId(plantsAndDevice.plant.getId()).observe((LifecycleOwner) activity, (List<Detection> detections) -> {
                    if (!detections.isEmpty()) {
                        if (detectionLayout.getVisibility() != View.VISIBLE) {
                            detectionLayout.setVisibility(View.VISIBLE);
                        }
                        for (Detection detection : detections) {
                            String text = getString(AttributeItem.valueOf(detection.getType()).getAttributeName()) + ": " + detection.valueToString();
                            if (AttributeItem.valueOf(detection.getType()) == AttributeItem.TEMPERATURE) {
                                lastTemperature.setText(text);
                                if (lastTemperatureIcon.getVisibility() != View.VISIBLE) {
                                    lastTemperatureIcon.setVisibility(View.VISIBLE);
                                }
                            } else if (AttributeItem.valueOf(detection.getType()) == AttributeItem.HUMIDITY) {
                                lastHumidity.setText(text);
                                if (lastHumidityIcon.getVisibility() != View.VISIBLE) {
                                    lastHumidityIcon.setVisibility(View.VISIBLE);
                                }
                            } else if (AttributeItem.valueOf(detection.getType()) == AttributeItem.BRIGHTNESS) {
                                lastBrightness.setText(text);
                                if (lastBrightnessIcon.getVisibility() != View.VISIBLE) {
                                    lastBrightnessIcon.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    } else {
                        if (detectionLayout.getVisibility() == View.VISIBLE) {
                            detectionLayout.setVisibility(View.GONE);
                        }
                    }
                });

            });

            viewModel.getSelected().observe((LifecycleOwner) activity, (Device device)-> deviceName.setText(device.getName()));

            Button editButton = view.findViewById(R.id.button_edit);
            editButton.setOnClickListener(new View.OnClickListener(){

                private boolean edit = false;

                @Override
                public void onClick(View v) {
                    if(!edit){
                        deviceName.setEnabled(true);
                        editButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_check_24,0,0,0);
                        edit = true;
                    }else{
                        viewModel.updateDeviceName(deviceName.getText().toString() , currentDevice.getId());
                        deviceName.setEnabled(false);
                        editButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_edit_24,0,0,0);
                        edit = false;
                    }
                }
            });

            Button change_plant = view.findViewById(R.id.change_plant);
            change_plant.setOnClickListener((View v)-> {
                ChangePlantFragment changePlantFragment = new ChangePlantFragment();
                changePlantFragment.setCurrentDevice(currentDevice);
                Utilities.insertFragemnt((AppCompatActivity) activity, changePlantFragment, "Change plant fragment");
            });

            Utilities.setUpToolbar((AppCompatActivity) activity, getString(R.string.my_devices));
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.app_bar_devices).setVisible(false);
        menu.findItem(R.id.app_bar_plants).setVisible(true);
    }

}

