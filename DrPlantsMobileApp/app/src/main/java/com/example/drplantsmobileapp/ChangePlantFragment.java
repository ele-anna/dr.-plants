package com.example.drplantsmobileapp;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.drplantsmobileapp.RecyclerView.AddPlantRecyclerView.OptimalAttributeValue;
import com.example.drplantsmobileapp.RecyclerView.DeviceRecyclerView.Device;
import com.example.drplantsmobileapp.RecyclerView.DeviceRecyclerView.PlantAdapter;
import com.example.drplantsmobileapp.RecyclerView.PlantsRecyclerView.PlantsCardItem;
import com.example.drplantsmobileapp.ViewModel.DeviceViewModel;
import com.google.android.material.checkbox.MaterialCheckBox;

import org.json.JSONObject;

import java.util.List;

import static com.example.drplantsmobileapp.Utilities.ADDRESS;


public class ChangePlantFragment extends Fragment {

    private final static String OSM_REQUEST_TAG = "OSM_REQUEST";
    private static final String TAG = AddDeviceFragment.class.getSimpleName();

    private PlantAdapter adapter;
    private RecyclerView recyclerView;
    private DeviceViewModel deviceViewModel;
    private Device currentDevice;
    private RequestQueue requestQueue;

    private void setRecyclerView(Activity activity)
    {
        this.recyclerView = getView().findViewById(R.id.plant_recycler_view);
        recyclerView.setHasFixedSize(true);
        adapter = new PlantAdapter(activity);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.change_plantdevice, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Activity activity = getActivity();

        if (activity != null) {
            this.requestQueue = Volley.newRequestQueue(activity);
            Button confirmButton = view.findViewById(R.id.confirm_button_change);
            confirmButton.setOnClickListener((View v) -> {
                MaterialCheckBox checkBox = this.adapter.getLastChecked();
                if (checkBox != null) {
                    int plantId = this.adapter.getPlantIdChecked();
                    this.deviceViewModel.associateDevice(this.currentDevice.getPlantAssociated(), null);
                    this.deviceViewModel.updatePlantAssociated(this.currentDevice.getId(), plantId);
                    this.deviceViewModel.associateDevice(plantId, new java.sql.Date(System.currentTimeMillis()).toString());
                    for (OptimalAttributeValue optimalAttributeValue : this.deviceViewModel.getPlantOptimalAttributeList(plantId)) {
                        this.sendVolleyRequestOptimalAttribute(String.valueOf(this.currentDevice.getIdESP()), optimalAttributeValue);
                    }
                    Utilities.insertFragemnt((AppCompatActivity) activity, new AssociatedPlantFragment(), "Device");
                } else {
                    Toast.makeText(getContext(), "Select a plant", Toast.LENGTH_SHORT).show();
                }
            });

            this.setRecyclerView(activity);
            this.deviceViewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(DeviceViewModel.class);
            this.deviceViewModel.getPlantsWithoutDevice().observe((LifecycleOwner) activity, (List<PlantsCardItem> plants)-> this.adapter.setData(plants));
            Utilities.setUpToolbar((AppCompatActivity) activity, getString(R.string.my_devices));

        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.app_bar_devices).setVisible(false);
        menu.findItem(R.id.app_bar_plants).setVisible(true);
    }

    public void setCurrentDevice(Device currentDevice) {
        this.currentDevice = currentDevice;
    }

    private void sendVolleyRequestOptimalAttribute(String deviceId, OptimalAttributeValue optimalAttribute) {
        Log.e(TAG, "SEND");
        String url = ADDRESS + "/api/data/optimalValue/";
        JSONObject data = new JSONObject();
        try {
            data.put("id", deviceId);
            data.put("attribute", optimalAttribute.getAttributeItem());
            data.put("minValue", Float.toString(optimalAttribute.getMinValue()));
            data.put("maxValue", Float.toString(optimalAttribute.getMaxValue()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Request a jsonObject response from the provided URL.
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, data, response -> Log.e(TAG, response.toString()), error -> Log.e(TAG, error.toString()));

        jsonObjectRequest.setTag(OSM_REQUEST_TAG);
        // Add the request to the RequestQueue.
        requestQueue.add(jsonObjectRequest);

    }

}


