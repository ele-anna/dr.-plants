package com.example.drplantsmobileapp.DataBase;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.drplantsmobileapp.Detection;
import com.example.drplantsmobileapp.RecyclerView.AddPlantRecyclerView.OptimalAttributeValue;
import com.example.drplantsmobileapp.RecyclerView.DeviceRecyclerView.Device;
import com.example.drplantsmobileapp.RecyclerView.NotificationsRecyclerView.Notification;
import com.example.drplantsmobileapp.RecyclerView.PlantsRecyclerView.PlantsCardItem;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {PlantsCardItem.class, Device.class, Notification.class, OptimalAttributeValue.class, Detection.class}, version = 1)
public abstract class DRPlantsDatabase extends RoomDatabase {
    public abstract PlantDAO plantDAO();
    public abstract DeviceDAO deviceDAO();
    public abstract NotificationDAO notificationDAO();
    public abstract OptimalAttributeDAO optimalAttributeDAO();
    public abstract DetectionDAO detectionDAO();

    private static volatile DRPlantsDatabase INSTANCE;
    private static final int NUMBER_OF_THREAD = 4;
    static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREAD);

    static DRPlantsDatabase getDatabase(final Context context){
        if(INSTANCE == null){
            synchronized (DRPlantsDatabase.class){
                if(INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), DRPlantsDatabase.class, "database").build();
                }
            }
        }
        return INSTANCE;
    }

}
