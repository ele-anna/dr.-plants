package com.example.drplantsmobileapp.DataBase;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.example.drplantsmobileapp.Detection;

import java.util.List;

@Dao
public interface DetectionDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addDetection(Detection detection);

    @Transaction
    @Query("SELECT * FROM detection WHERE plant_associated = :plantId AND detection_type = :attribute ORDER BY detection_id DESC LIMIT 20")
    LiveData<List<Detection>> getDetections(int plantId, String attribute);

    @Transaction
    @Query("SELECT * FROM detection GROUP BY detection_type HAVING plant_associated =:plantId ORDER BY detection_id DESC")
    LiveData<List<Detection>> getLastDetectionsByPlantId(int plantId);
}
