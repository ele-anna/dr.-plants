package com.example.drplantsmobileapp.DataBase;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.drplantsmobileapp.Detection;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class DetectionRepository {

    private final DRPlantsDatabase database;
    private final DetectionDAO detectionDAO;

    public DetectionRepository(Application application) {
        this.database = DRPlantsDatabase.getDatabase(application);
        this.detectionDAO = this.database.detectionDAO();
    }

    public LiveData<List<Detection>> getDetectionList(int plantId, String attribute) {
        final Callable<LiveData<List<Detection>>> getListCallable = () -> this.detectionDAO.getDetections(plantId, attribute);
        final Future<LiveData<List<Detection>>> future = DRPlantsDatabase.databaseWriteExecutor.submit(getListCallable);
        try {
            return future.get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    public LiveData<List<Detection>> getDetectionListByPlantId(int plantId) {
        final Callable<LiveData<List<Detection>>> getListCallable = () -> this.detectionDAO.getLastDetectionsByPlantId(plantId);
        final Future<LiveData<List<Detection>>> future = DRPlantsDatabase.databaseWriteExecutor.submit(getListCallable);
        try {
            LiveData<List<Detection>> list = future.get();
            return list;
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void addDetection(final Detection detection) {
        DRPlantsDatabase.databaseWriteExecutor.execute(() -> this.detectionDAO.addDetection(detection));
    }
}
