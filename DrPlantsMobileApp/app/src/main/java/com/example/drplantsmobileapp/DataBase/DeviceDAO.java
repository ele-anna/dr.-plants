package com.example.drplantsmobileapp.DataBase;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.example.drplantsmobileapp.RecyclerView.DeviceRecyclerView.Device;

import java.util.List;

@Dao
public interface DeviceDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addDevice(Device device);

    @Delete
    void deleteDevice(Device device);

    @Transaction
    @Query("SELECT * FROM device ORDER BY device_id DESC")
    LiveData<List<Device>> getDevices();

    @Transaction
    @Query("UPDATE device SET device_name = :name WHERE device_id = :deviceId")
    void updateDeviceName(String name, int deviceId);

    @Transaction
    @Query("UPDATE device SET plant_associated = :plantId WHERE device_id = :deviceId")
    void updatePlantAssociated(int deviceId, int plantId);

    @Transaction
    @Query("SELECT * FROM device, plant WHERE plant_id = plant_associated AND device_id =:deviceId")
    LiveData<PlantAndDevice> getPlantAssociated(int deviceId);

    @Transaction
    @Query("SELECT * FROM device WHERE device_connection=:deviceEsp")
    List<Device> getConnection(int deviceEsp);

    @Transaction
    @Query("SELECT device_connection FROM device")
    List<Integer> getDevicesIdEsp();
}
