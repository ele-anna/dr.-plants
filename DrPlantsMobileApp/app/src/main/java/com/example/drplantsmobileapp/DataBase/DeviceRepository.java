package com.example.drplantsmobileapp.DataBase;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.drplantsmobileapp.RecyclerView.DeviceRecyclerView.Device;
import com.example.drplantsmobileapp.RecyclerView.PlantsRecyclerView.PlantsCardItem;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class DeviceRepository {

    private final DRPlantsDatabase database;
    private final DeviceDAO deviceDAO;
    private final LiveData<List<Device>> deviceList;
    private final LiveData<List<PlantsCardItem>> plantsWithoutDevice;


    public DeviceRepository(Application application) {
        this.database = DRPlantsDatabase.getDatabase(application);
        this.deviceDAO = this.database.deviceDAO();
        this.deviceList = this.database.deviceDAO().getDevices();
        this.plantsWithoutDevice = this.database.plantDAO().getPlantWithoutDevice();
    }

    public LiveData<List<Device>> getDeviceList() {
        return deviceList;
    }

    public void addDevice(final Device device) {
        DRPlantsDatabase.databaseWriteExecutor.execute(() -> this.deviceDAO.addDevice(device));
    }

    public void associateDevice(int plantId, String date) {
        DRPlantsDatabase.databaseWriteExecutor.execute(() -> this.database.plantDAO().associateDevice(plantId, date));
    }

    public LiveData<List<PlantsCardItem>> getPlantsWithoutDevice(){ return this.plantsWithoutDevice; }

    public void updateDeviceName(String name, int id) {
        DRPlantsDatabase.databaseWriteExecutor.execute(() -> this.database.deviceDAO().updateDeviceName(name, id));
    }

    public void updatePlantAssociated(int deviceId, int plantId) {
        DRPlantsDatabase.databaseWriteExecutor.execute(() -> this.database.deviceDAO().updatePlantAssociated(deviceId, plantId));
    }

    public LiveData<List<PlantsCardItem>> getPlantById(int plantId){
        final Callable<LiveData<List<PlantsCardItem>>> getListCallable = () -> this.database.plantDAO().getPlantById(plantId);
        final Future<LiveData<List<PlantsCardItem>>> future = DRPlantsDatabase.databaseWriteExecutor.submit(getListCallable);
        try {
            return future.get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    public LiveData<PlantAndDevice> getPlantAssociated(int deviceId){
        final Callable<LiveData<PlantAndDevice>> getListCallable = () -> this.deviceDAO.getPlantAssociated(deviceId);
        final Future<LiveData<PlantAndDevice>> future = DRPlantsDatabase.databaseWriteExecutor.submit(getListCallable);
        try {
            return future.get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void deleteDevice(Device device, int plantAssociated) {
        DRPlantsDatabase.databaseWriteExecutor.execute(() -> {
            database.plantDAO().associateDevice(plantAssociated, null);
            database.deviceDAO().deleteDevice(device);
        });
    }

    public List<Device> getConnection(int deviceEsp) {
        final Callable<List<Device>> getListCallable = () -> this.deviceDAO.getConnection(deviceEsp);
        final Future<List<Device>> future = DRPlantsDatabase.databaseWriteExecutor.submit(getListCallable);
        try {
            return future.get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }
}
