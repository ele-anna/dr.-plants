package com.example.drplantsmobileapp.DataBase;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.example.drplantsmobileapp.RecyclerView.NotificationsRecyclerView.Notification;

import java.util.List;

@Dao
public interface NotificationDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addNotification(Notification notification);

    @Delete
    void deleteNotification(Notification notification);

    @Transaction
    @Query("SELECT * from notification ORDER BY notification_id DESC")
    LiveData<List<Notification>> getNotifications();
}
