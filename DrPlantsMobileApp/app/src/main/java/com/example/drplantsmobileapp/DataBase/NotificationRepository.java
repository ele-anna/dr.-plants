package com.example.drplantsmobileapp.DataBase;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.drplantsmobileapp.RecyclerView.NotificationsRecyclerView.Notification;

import java.util.List;

public class NotificationRepository {

    private final DRPlantsDatabase database;
    private final NotificationDAO notificationDAO;
    private final LiveData<List<Notification>> notificationList;


    public NotificationRepository(Application application) {
        this.database = DRPlantsDatabase.getDatabase(application);
        this.notificationDAO = this.database.notificationDAO();
        this.notificationList = this.notificationDAO.getNotifications();
    }

    public LiveData<List<Notification>> getNotificationList() {
        return this.notificationList;
    }

    public void addNotification(Notification notification) {
        DRPlantsDatabase.databaseWriteExecutor.execute(() -> this.notificationDAO.addNotification(notification));
    }

    public void deleteNotification(Notification notification) {
        DRPlantsDatabase.databaseWriteExecutor.execute(() -> this.notificationDAO.deleteNotification(notification));
    }
}
