package com.example.drplantsmobileapp.DataBase;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.example.drplantsmobileapp.RecyclerView.AddPlantRecyclerView.OptimalAttributeValue;

import java.util.List;

@Dao
public interface OptimalAttributeDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addOptimalAttribute(OptimalAttributeValue optimalAttributeValue);

    @Transaction
    @Query("SELECT * FROM optimal_attribute WHERE plant_associated=:plantId ORDER BY attribute_id DESC")
    List<OptimalAttributeValue> getPlantOptimalAttributeValue(int plantId);

    @Transaction
    @Query("SELECT * FROM optimal_attribute WHERE plant_associated=:plantId AND attribute_item=:attributeType ORDER BY attribute_id DESC")
    LiveData<OptimalAttributeValue> getOptimalAttributeValueByPlantId(int plantId, String attributeType);

    @Transaction
    @Query("DELETE FROM optimal_attribute WHERE plant_associated=:plantId")
    void deleteOptimalAttributePlants(int plantId);
}
