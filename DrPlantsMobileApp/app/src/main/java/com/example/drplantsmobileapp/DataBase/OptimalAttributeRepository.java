package com.example.drplantsmobileapp.DataBase;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.drplantsmobileapp.RecyclerView.AddPlantRecyclerView.OptimalAttributeValue;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class OptimalAttributeRepository {

    private final DRPlantsDatabase database;
    private final OptimalAttributeDAO optimalAttributeDAO;


    public OptimalAttributeRepository(Application application) {
        this.database = DRPlantsDatabase.getDatabase(application);
        this.optimalAttributeDAO = this.database.optimalAttributeDAO();
    }

    public LiveData<OptimalAttributeValue> getSpecificOptimalAttribute(int plantId, String attributeType) {
        final Callable<LiveData<OptimalAttributeValue>> getListCallable = () -> this.database.optimalAttributeDAO().getOptimalAttributeValueByPlantId(plantId, attributeType);
        final Future<LiveData<OptimalAttributeValue>> future = DRPlantsDatabase.databaseWriteExecutor.submit(getListCallable);
        try {
            return future.get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<OptimalAttributeValue> getPlantOptimalAttribute(int plantId) {
        final Callable<List<OptimalAttributeValue>> getListCallable = () -> this.database.optimalAttributeDAO().getPlantOptimalAttributeValue(plantId);
        final Future<List<OptimalAttributeValue>> future = DRPlantsDatabase.databaseWriteExecutor.submit(getListCallable);
        try {
            return future.get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void addOptimalAttribute(final OptimalAttributeValue optimalAttributeValue) {
        DRPlantsDatabase.databaseWriteExecutor.execute(() -> this.optimalAttributeDAO.addOptimalAttribute(optimalAttributeValue));
    }

}
