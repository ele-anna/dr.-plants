package com.example.drplantsmobileapp.DataBase;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.example.drplantsmobileapp.RecyclerView.DeviceRecyclerView.Device;
import com.example.drplantsmobileapp.RecyclerView.PlantsRecyclerView.PlantsCardItem;

public class PlantAndDevice {
    @Embedded
    public Device device;

    @Relation(
            parentColumn = "plant_associated",
            entityColumn = "plant_id"
    )
    public PlantsCardItem plant;
}
