package com.example.drplantsmobileapp.DataBase;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.example.drplantsmobileapp.RecyclerView.NotificationsRecyclerView.Notification;
import com.example.drplantsmobileapp.RecyclerView.PlantsRecyclerView.PlantsCardItem;

import java.util.List;

public class PlantAndNotification {
    @Embedded
    public PlantsCardItem plant;

    @Relation(
            parentColumn = "plant_id",
            entityColumn = "plant_associated"
    )
    public List<Notification> notifications;

}
