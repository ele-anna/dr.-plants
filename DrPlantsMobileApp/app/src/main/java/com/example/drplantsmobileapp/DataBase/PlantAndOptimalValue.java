package com.example.drplantsmobileapp.DataBase;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.example.drplantsmobileapp.RecyclerView.AddPlantRecyclerView.OptimalAttributeValue;
import com.example.drplantsmobileapp.RecyclerView.PlantsRecyclerView.PlantsCardItem;

import java.util.List;

public class PlantAndOptimalValue {

    @Embedded
    public PlantsCardItem plantsCardItem;

    @Relation(
            parentColumn = "plant_id",
            entityColumn = "plant_associated"
    )
    public List<OptimalAttributeValue> optimalAttributeValueList;
}
