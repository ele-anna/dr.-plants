package com.example.drplantsmobileapp.DataBase;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Delete;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.example.drplantsmobileapp.RecyclerView.PlantsRecyclerView.PlantsCardItem;

import java.util.List;

@Dao
public interface PlantDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long addPlant(PlantsCardItem plantsCardItem);

    @Delete
    void deletePlant(PlantsCardItem plant);

    @Transaction
    @Query("SELECT * FROM plant ORDER BY plant_id DESC")
    LiveData<List<PlantsCardItem>> getPlants();

    @Transaction
    @Query("UPDATE plant SET date_of_association = :date WHERE plant_id = :plantId")
    void associateDevice(int plantId, String date);


    @Transaction
    @Query("SELECT * FROM plant WHERE date_of_association IS NULL")
    LiveData<List<PlantsCardItem>> getPlantWithoutDevice();

    @Transaction
    @Query("SELECT * FROM plant WHERE plant_id = :plantId")
    LiveData<List<PlantsCardItem>> getPlantById(int plantId);

    @Query("SELECT plant_id FROM plant WHERE plant_name = :plantName")
    LiveData<Integer> getPlantIdByName(String plantName);

    @Transaction
    @Query("SELECT * FROM device, plant WHERE plant_id = plant_associated AND plant_id =:plantId")
    PlantAndDevice getPlantAssociated(long plantId);

}
