package com.example.drplantsmobileapp.DataBase;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.drplantsmobileapp.RecyclerView.PlantsRecyclerView.PlantsCardItem;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class PlantRepository {

    private final PlantDAO plantDAO;
    private final LiveData<List<PlantsCardItem>> plantList;
    private final DRPlantsDatabase database;

    public PlantRepository(Application application) {
        this.database = DRPlantsDatabase.getDatabase(application);
        this.plantDAO = database.plantDAO();
        this.plantList = database.plantDAO().getPlants();
    }

    public LiveData<List<PlantsCardItem>> getPlantList() {
        return plantList;
    }

    public long addPlant(final PlantsCardItem plant) {
        Callable<Long> insertCallable = () -> this.plantDAO.addPlant(plant);
        Long plantId = (long) -1;

        final Future<Long> future = DRPlantsDatabase.databaseWriteExecutor.submit(insertCallable);
        try {
            plantId = future.get();
        } catch (InterruptedException | ExecutionException e1) {
            e1.printStackTrace();
        }
        return plantId;
    }
    
    public void deletePlant(PlantsCardItem plant) {
        DRPlantsDatabase.databaseWriteExecutor.execute(() -> {
            database.optimalAttributeDAO().deleteOptimalAttributePlants(plant.getId());
            this.plantDAO.deletePlant(plant);
        });
    }
}
