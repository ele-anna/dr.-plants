package com.example.drplantsmobileapp;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "detection")
public class Detection {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "detection_id")
    private int id;
    @ColumnInfo(name = "detection_type")
    private final String type;
    @ColumnInfo(name = "data")
    private final String data;
    @ColumnInfo(name = "value")
    private final float value;
    @ColumnInfo(name = "unit_measure")
    private final String unitOfMeasure;
    @ColumnInfo(name = "plant_associated")
    private final int plantAssociated;

    public Detection(String type, String data, float value, String unitOfMeasure, int plantAssociated) {
        this.type = type;
        this.data = data;
        this.value = value;
        this.unitOfMeasure = unitOfMeasure;
        this.plantAssociated = plantAssociated;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public String getData() {
        return data;
    }

    public float getValue() {
        return value;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public int getPlantAssociated() {
        return plantAssociated;
    }

    public String valueToString() {
        return this.value + " " + this.unitOfMeasure;
    }
}
