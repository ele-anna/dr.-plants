package com.example.drplantsmobileapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class DeviceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device);

        if(savedInstanceState == null) {
            Utilities.insertFragemnt(this, new DeviceFragment(), "DeviceFragment");
        }

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.isVisible()) {
            if (item.getItemId() == R.id.app_bar_devices) {
                Utilities.insertFragemnt(this, new PlantFragment(), PlantFragment.class.getSimpleName());
                return true;
            } else if (item.getItemId() == R.id.app_bar_plants) {
                Intent intent = new Intent(this, MainActivity.class);
                this.startActivity(intent);
                return true;
            } else if (item.getItemId() == R.id.app_bar_notifications) {
                Intent intent = new Intent(this, NotificationsActivity.class);
                this.startActivity(intent);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_app_bar, menu);
        return true;
    }
}
