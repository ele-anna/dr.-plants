package com.example.drplantsmobileapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.RecyclerView;

import com.example.drplantsmobileapp.RecyclerView.DeviceRecyclerView.Device;
import com.example.drplantsmobileapp.RecyclerView.DeviceRecyclerView.DeviceAdapter;
import com.example.drplantsmobileapp.RecyclerView.OnItemListener;
import com.example.drplantsmobileapp.ViewModel.DeviceViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class DeviceFragment extends Fragment implements OnItemListener {

    private DeviceAdapter adapter;
    private RecyclerView recyclerView;
    private DeviceViewModel viewModel;

    private void setRecyclerView(Activity activity)
    {
        this.recyclerView = requireView().findViewById(R.id.device_recycler_view);
        this.recyclerView.setHasFixedSize(true);
        this.adapter = new DeviceAdapter(activity, this);
        this.recyclerView.setAdapter(this.adapter);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.device_menager, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Activity activity = getActivity();

        if (activity != null) {

            FloatingActionButton addDevice = view.findViewById(R.id.fab_add);

            addDevice.setOnClickListener((View v)->Utilities.insertFragemnt((AppCompatActivity) activity, new AddDeviceFragment(), "Add device fragment"));

            this.setRecyclerView(activity);

            this.viewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(DeviceViewModel.class);
            this.viewModel.getDevices().observe((LifecycleOwner) activity, (List<Device> devices)-> this.adapter.setData(devices));

            Utilities.setUpToolbar((AppCompatActivity) activity, getString(R.string.my_devices));
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.app_bar_devices).setVisible(false);
        menu.findItem(R.id.app_bar_plants).setVisible(true);
    }

    @Override
    public void onItemClick(int position) {
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();

        if (appCompatActivity != null) {
            this.viewModel.select(this.viewModel.getDevice(position));
            Utilities.insertFragemnt(appCompatActivity, new AssociatedPlantFragment(),
                    "Associated plant fragment");
        }
    }

    @Override
    public boolean onItemLongClick(int position) {
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();

        new AlertDialog.Builder(appCompatActivity)
                .setTitle(R.string.are_you_sure)
                .setMessage(R.string.delete)
                .setPositiveButton(R.string.yes, (DialogInterface d, int which)->{
                    Device device = this.viewModel.getDevice(position);
                    viewModel.deleteDevice(device, device.getPlantAssociated());
                })
                .setNegativeButton(R.string.no, null)
                .show();
        return true;
    }
}
