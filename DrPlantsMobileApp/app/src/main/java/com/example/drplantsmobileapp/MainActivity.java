package com.example.drplantsmobileapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.drplantsmobileapp.Service.Service;
import com.example.drplantsmobileapp.ViewModel.AddPlantViewModel;

import static com.example.drplantsmobileapp.Utilities.REQUEST_IMAGE_CAPTURE;

public class MainActivity extends AppCompatActivity {

    private AddPlantViewModel addPlantViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device);

        if (savedInstanceState == null) {
            Utilities.insertFragemnt(this, new PlantFragment(), PlantFragment.class.getSimpleName());
        }

        this.addPlantViewModel = new ViewModelProvider(this).get(AddPlantViewModel.class);
        startService(new Intent(this, Service.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_app_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.isVisible()) {
            if (item.getItemId() == R.id.app_bar_plants) {
                Utilities.insertFragemnt(this, new PlantFragment(), PlantFragment.class.getSimpleName());
                return true;
            } else if (item.getItemId() == R.id.app_bar_devices) {
                Intent intent = new Intent(this, DeviceActivity.class);
                this.startActivity(intent);
                return true;
            } else if (item.getItemId() == R.id.app_bar_notifications) {
                Intent intent = new Intent(this, NotificationsActivity.class);
                this.startActivity(intent);
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            if (data != null) {
                final Bundle extras = data.getExtras();
                if (extras != null) {
                    final Bitmap bitmap = (Bitmap) extras.get("data");
                    addPlantViewModel.setImageBitmap(bitmap);
                    Utilities.insertFragemnt(this, new AddPlantFragment(), AddPlantFragment.class.getSimpleName());
                }
            }
        }
    }
}