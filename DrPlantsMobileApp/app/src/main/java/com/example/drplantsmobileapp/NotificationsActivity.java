package com.example.drplantsmobileapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class NotificationsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        if (savedInstanceState == null)
            Utilities.insertFragemnt(this, new NotificationsFragment(),
                    NotificationsFragment.class.getSimpleName());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_app_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.isVisible()) {
            if (item.getItemId() == R.id.app_bar_plants) {
                Intent intent = new Intent(this, MainActivity.class);
                this.startActivity(intent);
                return true;
            } else if (item.getItemId() == R.id.app_bar_devices) {
                Intent intent = new Intent(this, DeviceActivity.class);
                this.startActivity(intent);
                return true;
            }
        }
        return false;
    }
}
