package com.example.drplantsmobileapp;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.example.drplantsmobileapp.RecyclerView.NotificationsRecyclerView.NotificationAdapter;
import com.example.drplantsmobileapp.RecyclerView.NotificationsRecyclerView.SwipeToDelete;
import com.example.drplantsmobileapp.ViewModel.NotificationListViewModel;

public class NotificationsFragment extends Fragment {

    private static final String TAG = NotificationsFragment.class.getSimpleName();
    private NotificationAdapter notificationAdapter;
    private ItemTouchHelper helper;
   private NotificationListViewModel notificationListViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.notifications, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Activity activity = getActivity();
        if (activity != null) {
            Utilities.setUpToolbar((AppCompatActivity) activity, getString(R.string.notifications));
            this.notificationListViewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(NotificationListViewModel.class);
            notificationListViewModel.getNotifications().observe((LifecycleOwner) activity, notifications -> notificationAdapter.setData(notifications));
            setRecyclerView(activity);
        } else {
            Log.d(TAG, "Error, activity is null");
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.app_bar_plants).setVisible(true);
        menu.findItem(R.id.app_bar_notifications).setVisible(false);
    }

    private void setRecyclerView(final Activity activity) {
        final RecyclerView recyclerView = requireView().findViewById(R.id.notifications_recycler_view);
        recyclerView.setHasFixedSize(true);
        this.notificationAdapter = new NotificationAdapter(activity);
        this.helper = new ItemTouchHelper(new SwipeToDelete(this.notificationAdapter, this.notificationListViewModel));
        helper.attachToRecyclerView(recyclerView);
        recyclerView.setAdapter(this.notificationAdapter);
    }
}
