package com.example.drplantsmobileapp;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import com.example.drplantsmobileapp.RecyclerView.AddPlantRecyclerView.OptimalAttributeValue;
import com.example.drplantsmobileapp.RecyclerView.PlantattributeRecyclerView.AttributeItem;
import com.example.drplantsmobileapp.RecyclerView.PlantsRecyclerView.PlantsCardItem;
import com.example.drplantsmobileapp.ViewModel.OptimalAttributeModel;
import com.example.drplantsmobileapp.ViewModel.PlantAttributeViewModel;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.EntryXComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ir.androidexception.datatable.DataTable;
import ir.androidexception.datatable.model.DataTableHeader;
import ir.androidexception.datatable.model.DataTableRow;

public class PlantAttributeFragment extends Fragment {

    private static final String TAG = PlantAttributeFragment.class.getSimpleName();
    private DataTable dataTable;
    private LineChart dataChart;
    private AttributeItem currentAttribute;
    private TextView actualValue;
    private TextView maxValue;
    private TextView minValue;
    private ImageView attributeImage;
    private PlantsCardItem currentPlant;
    private OptimalAttributeValue optimalAttributeValue;
    private TextView detectionState;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.detection_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Activity activity = getActivity();
        if (activity != null) {
            this.dataTable = activity.findViewById(R.id.values_table);
            this.dataChart = activity.findViewById(R.id.values_chart);
            this.initTable();

            this.actualValue = activity.findViewById(R.id.actual_value);
            this.minValue = activity.findViewById(R.id.value_min);
            this.maxValue = activity.findViewById(R.id.value_max);
            this.detectionState = activity.findViewById(R.id.monitoring_state);
            this.attributeImage = activity.findViewById(R.id.monitoring_attribute_image);

            OptimalAttributeModel optimalAttributeModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(OptimalAttributeModel.class);
            PlantAttributeViewModel plantAttributeViewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(PlantAttributeViewModel.class);
            plantAttributeViewModel.getSelectedPlantAndAttribute().observe(getViewLifecycleOwner(), select -> {
                Utilities.setUpToolbar((AppCompatActivity) activity, getString(select.second.getAttributeName()));
                this.currentPlant = select.first;
                this.currentAttribute = select.second;
                optimalAttributeModel.getSpecificOptimalAttributeValue(this.currentPlant.getId(), this.currentAttribute.name()).observe((LifecycleOwner) activity, optimalAttributeValue -> {
                    String minText = getString(R.string.min) + " " + optimalAttributeValue.minValueToString();
                    String maxText = getString(R.string.max) + " " + optimalAttributeValue.maxValueToString();

                    this.minValue.setText(minText);
                    this.maxValue.setText(maxText);

                    final String image_path = AttributeItem.valueOf(optimalAttributeValue.getAttributeItem()).getAttributeImage();
                    final Drawable drawable = ContextCompat.getDrawable(activity, activity.getResources().getIdentifier(image_path, "drawable", activity.getPackageName()));
                    this.attributeImage.setImageDrawable(drawable);
                });
            });

            plantAttributeViewModel.getOptimalValue().observe((LifecycleOwner) activity, o -> optimalAttributeValue = o);
            plantAttributeViewModel.getDetectionList().observe((LifecycleOwner) activity, (List<Detection> detections) -> {
                if (detections != null && (!detections.isEmpty())) {
                    Detection lastValue = detections.get(0);
                    this.actualValue.setText(lastValue.valueToString());
                    if (lastValue.getValue() <= optimalAttributeValue.getMinValue()) {
                        this.detectionState.setText(R.string.low);
                    } else if (lastValue.getValue() >= optimalAttributeValue.getMaxValue()) {
                        this.detectionState.setText(R.string.high);
                    } else {
                        this.detectionState.setText(R.string.ok);
                    }
//                    this.actualValue.setTextColor(this.currentAttribute.getColor());
                    this.setDetectionTableData(activity, detections);
                    this.initChart(activity, detections);
                }
            });
        } else {
            Log.e(TAG, "Error, activity is null");
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.app_bar_plants).setVisible(true);
    }

    private void initChart(Activity activity, List<Detection> detections) {
        final LineData data = this.getData(activity, detections);
        ((LineDataSet) data.getDataSetByIndex(0)).setCircleHoleColor(this.currentAttribute.getColor());

        this.dataChart.getDescription().setEnabled(false);
        this.dataChart.setDrawGridBackground(false);
        this.dataChart.setTouchEnabled(true);
        this.dataChart.setDragEnabled(true);
        this.dataChart.setScaleEnabled(true);
        this.dataChart.setPinchZoom(true);
        this.dataChart.setBackgroundColor(Color.TRANSPARENT);
        this.dataChart.setData(data);
        Legend l = this.dataChart.getLegend();
        l.setEnabled(true);

        this.dataChart.invalidate();
    }

    private LineData getData(Activity activity, List<Detection> detections) {

        ArrayList<Entry> values = new ArrayList<>();

        int i = 0;
        for (Detection detection : detections) {
            values.add(new Entry(i++, detection.getValue(), detection));
        }
        values.sort(new EntryXComparator());

        LineDataSet set1 = new LineDataSet(values, activity.getString(this.currentAttribute.getAttributeName()));
        set1.setDrawFilled(true);
        set1.setFillColor(this.currentAttribute.getColor());

        set1.setLineWidth(1.75f);
        set1.setCircleRadius(5f);
        set1.setCircleHoleRadius(2.5f);
        set1.setColor(this.currentAttribute.getColor());
        set1.setCircleColor(this.currentAttribute.getColor());
        set1.setHighLightColor(this.currentAttribute.getColor());
        set1.setDrawValues(false);
        return new LineData(set1);
    }

    private void initTable() {
        final DataTableHeader header = new DataTableHeader.Builder()
                .item("", 1)
                .item(getString(R.string.value), 1)
                .item(getString(R.string.data), 1)
                .build();
        dataTable.setTypeface(Typeface.DEFAULT);
        dataTable.setHeader(header);
    }

    private void setDetectionTableData(Activity activity, List<Detection> detections) {
        if (detections != null) {
            final ArrayList<DataTableRow> rows = new ArrayList<>();
            Collections.reverse(detections);
            int count = 1;
            for (Detection detection : detections) {
                final DataTableRow row = new DataTableRow.Builder()
                        .value("#"+ count)
                        .value(detection.valueToString())
                        .value(detection.getData())
                        .build();
                rows.add(row);
                count+=1;
            }
            dataTable.setRows(rows);
            dataTable.inflate(activity);
        }
    }

}
