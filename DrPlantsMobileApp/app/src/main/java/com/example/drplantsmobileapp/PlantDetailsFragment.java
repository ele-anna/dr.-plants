package com.example.drplantsmobileapp;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.RecyclerView;

import com.example.drplantsmobileapp.RecyclerView.OnItemListener;
import com.example.drplantsmobileapp.RecyclerView.PlantattributeRecyclerView.AttributeCardAdapter;
import com.example.drplantsmobileapp.RecyclerView.PlantattributeRecyclerView.AttributeItem;
import com.example.drplantsmobileapp.ViewModel.PlantAttributeViewModel;
import com.example.drplantsmobileapp.ViewModel.PlantsListViewModel;

public class PlantDetailsFragment extends Fragment implements OnItemListener {

    private static final String TAG = PlantDetailsFragment.class.getSimpleName();
    private PlantAttributeViewModel plantAttributeViewModel;
    private AttributeCardAdapter attributeCardAdapter;
    private TextView plantDetailsName;
    private ImageView plantDetailsImage;
    private TextView plantDetailsDescription;
    private ImageView plantDetailsDevice;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.plant_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        plantDetailsName = view.findViewById(R.id.details_plant_name);
        plantDetailsImage = view.findViewById(R.id.details_plant_image);
        plantDetailsDescription = view.findViewById(R.id.details_plant_description);
        plantDetailsDevice = view.findViewById(R.id.details_plant_device);

        final Activity activity = getActivity();
        if (activity != null) {
            Utilities.setUpToolbar((AppCompatActivity) activity, getString(R.string.plant_details));
            setRecyclerView(activity);

            this.plantAttributeViewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(PlantAttributeViewModel.class);
            this.plantAttributeViewModel.getAttributeItems().observe((LifecycleOwner) activity, plantAttributeItems -> attributeCardAdapter.setData(plantAttributeItems));
            final PlantsListViewModel listViewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(PlantsListViewModel.class);
            listViewModel.getSelected().observe(getViewLifecycleOwner(), plantsCardItem -> {
                this.plantAttributeViewModel.setCurrentPlant(plantsCardItem);
                plantDetailsName.setText(plantsCardItem.getPlantName());
                plantDetailsDescription.setText(plantsCardItem.getPlantDescription());
                final int deviceVisibility = plantsCardItem.getDate_association() != null ? View.VISIBLE : View.GONE;
                plantDetailsDevice.setVisibility(deviceVisibility);
                final String image_path = plantsCardItem.getImageResource();

                if (image_path.contains("ic_")) {
                    final Drawable drawable = ContextCompat.getDrawable(activity, activity.getResources().getIdentifier(image_path, "drawable", activity.getPackageName()));
                    this.plantDetailsImage.setImageDrawable(drawable);
                } else {
                    final Bitmap bitmap = Utilities.getImageBitmap(activity, Uri.parse(image_path));
                    if (bitmap != null) {
                        this.plantDetailsImage.setImageBitmap(bitmap);
                    }
                }
            });
        } else {
            Log.d(TAG, "Error, activity is null");
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.app_bar_plants).setVisible(true);
    }

    @Override
    public void onItemClick(int position) {
        final AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        if (appCompatActivity != null) {
            final AttributeItem attributeItem = plantAttributeViewModel.getAttributeItem(position);
            this.plantAttributeViewModel.selectAttribute(attributeItem);
            Utilities.insertFragemnt(appCompatActivity, new PlantAttributeFragment(), PlantAttributeFragment.class.getSimpleName());
        }
    }

    @Override
    public boolean onItemLongClick(int position) {
        return false;
    }

    private void setRecyclerView(Activity activity) {
        RecyclerView recyclerView = requireView().findViewById(R.id.my_plants_attribute_recycler_view);
        recyclerView.setNestedScrollingEnabled(false);
        final OnItemListener listener = this;
        attributeCardAdapter = new AttributeCardAdapter(activity, listener);
        recyclerView.setAdapter(attributeCardAdapter);
    }

}
