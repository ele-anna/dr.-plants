package com.example.drplantsmobileapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.RecyclerView;

import com.example.drplantsmobileapp.RecyclerView.OnItemListener;
import com.example.drplantsmobileapp.RecyclerView.PlantsRecyclerView.PlantsCardAdapter;
import com.example.drplantsmobileapp.RecyclerView.PlantsRecyclerView.PlantsCardItem;
import com.example.drplantsmobileapp.ViewModel.PlantsListViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import static com.example.drplantsmobileapp.Utilities.REQUEST_IMAGE_CAPTURE;

public class PlantFragment extends Fragment implements OnItemListener {

    private PlantsCardAdapter plantsCardAdapter;
    private PlantsListViewModel plantsListViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.plants_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Activity activity = getActivity();
        if (activity != null) {

            Utilities.setUpToolbar((AppCompatActivity) activity, getString(R.string.my_plants));
            setRecyclerView(activity);

            plantsListViewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(PlantsListViewModel.class);
            plantsListViewModel.getPlantsCardItems().observe((LifecycleOwner) activity, plantsCardItems -> plantsCardAdapter.setData(plantsCardItems));

            final FloatingActionButton floatingActionButton = view.findViewById(R.id.fab_add);
            floatingActionButton.setOnClickListener((v -> {
                final Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
                    activity.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }));
        }
    }

    private void setRecyclerView(final Activity activity) {
        RecyclerView recyclerView = requireView().findViewById(R.id.my_plants_recycler_view);
        recyclerView.setHasFixedSize(true);
        final OnItemListener listener = this;
        plantsCardAdapter = new PlantsCardAdapter(activity, listener);
        recyclerView.setAdapter(plantsCardAdapter);
    }

    @Override
    public void onItemClick(int position) {
        final AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        if (appCompatActivity != null) {
            plantsListViewModel.select(plantsListViewModel.getPlantsCardItem(position));

            Utilities.insertFragemnt(appCompatActivity, new PlantDetailsFragment(), PlantDetailsFragment.class.getSimpleName());
        }
    }

    @Override
    public boolean onItemLongClick(int position) {
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();

        new AlertDialog.Builder(appCompatActivity)
                .setTitle(R.string.are_you_sure)
                .setMessage(R.string.delete)
                .setPositiveButton(R.string.yes, (DialogInterface d, int which)->{
                    PlantsCardItem plant = this.plantsListViewModel.getPlantsCardItem(position);
                    if(plant.getDate_association() != null){
                        Toast.makeText(appCompatActivity.getApplicationContext(), "You can delete this plant, there is a device associated to it", Toast.LENGTH_LONG).show();
                    }else{
                        this.plantsListViewModel.deletePlant(plant);
                    }
                })
                .setNegativeButton(R.string.no, null)
                .show();
        return true;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.app_bar_plants).setVisible(false);
    }
}
