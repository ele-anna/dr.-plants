package com.example.drplantsmobileapp.RecyclerView.AddPlantRecyclerView;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;


import com.example.drplantsmobileapp.R;
import com.example.drplantsmobileapp.RecyclerView.PlantattributeRecyclerView.AttributeItem;
import com.example.drplantsmobileapp.Utilities;

import java.util.ArrayList;
import java.util.List;

public class OptimalAttributeAdapter extends RecyclerView.Adapter<OptimalAttributeViewHolder> {

    private List<OptimalAttributeValue> optimalAttributeValueList;
    private final Activity activity;

    public OptimalAttributeAdapter(Activity activity) {
        this.optimalAttributeValueList = new ArrayList<>();
        this.activity = activity;
    }

    @NonNull
    @Override
    public OptimalAttributeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new OptimalAttributeViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull OptimalAttributeViewHolder holder, int position) {
        final OptimalAttributeValue currentOptimalAttributeValue = optimalAttributeValueList.get(position);
        final String attributeItem = currentOptimalAttributeValue.getAttributeItem();
        String imagePath = AttributeItem.valueOf(attributeItem).getAttributeImage();
        if (imagePath.contains("ic_")) {
            final Drawable drawable = ContextCompat.getDrawable(activity, activity.getResources().getIdentifier(imagePath, "drawable", activity.getPackageName()));
            holder.getAttributeImage().setImageDrawable(drawable);
        } else {
            final Bitmap bitmap = Utilities.getImageBitmap(activity, Uri.parse(imagePath));
            if (bitmap != null) {
                holder.getAttributeImage().setImageBitmap(bitmap);
            }
        }
        holder.getAttributeOptimalValue().setText(currentOptimalAttributeValue.attributeString(activity));
    }

    @Override
    public int getItemCount() {
        return optimalAttributeValueList.size();
    }

    public void setData(List<OptimalAttributeValue> list) {
        this.optimalAttributeValueList = list;
        notifyDataSetChanged();
    }
}
