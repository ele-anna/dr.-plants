package com.example.drplantsmobileapp.RecyclerView.AddPlantRecyclerView;

import android.app.Activity;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.drplantsmobileapp.RecyclerView.PlantattributeRecyclerView.AttributeItem;

import org.jetbrains.annotations.NotNull;

@Entity(tableName = "optimal_attribute")
public class OptimalAttributeValue {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "attribute_id")
    private int id;
    @ColumnInfo(name = "attribute_item")
    private final String attributeItem;
    @ColumnInfo(name = "attribute_min_value")
    private final float minValue;
    @ColumnInfo(name = "attribute_max_value")
    private final float maxValue;
    @ColumnInfo(name = "unitofMeasure")
    private final String unitOfMeasure;
    @ColumnInfo(name = "plant_associated")
    private Long plantAssociated;

    public OptimalAttributeValue(String attributeItem, float minValue, float maxValue, String unitOfMeasure, Long plantAssociated) {
        this.attributeItem = attributeItem;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.unitOfMeasure = unitOfMeasure;
        this.plantAssociated = plantAssociated;
    }

    public String getAttributeItem() {
        return this.attributeItem;
    }

    public float getMinValue() {
        return this.minValue;
    }

    public float getMaxValue() {
        return this.maxValue;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public int getId() { return this.id; }

    public void setId(int id) { this.id = id; }

    public Long getPlantAssociated() {
        return plantAssociated;
    }

    public void setPlantAssociated(Long plantAssociated) {
        this.plantAssociated = plantAssociated;
    }

    public String attributeString(@NotNull Activity activity) {
        return activity.getString(AttributeItem.valueOf(this.attributeItem).getAttributeName()) + ": " +
                this.minValue + " " + this.unitOfMeasure + " - " +
                this.maxValue + " " + this.unitOfMeasure;
    }

    public String minValueToString() {
        return this.minValue + " " + this.unitOfMeasure;
    }

    public String maxValueToString() {
        return this.maxValue + " " + this.unitOfMeasure;
    }

}
