package com.example.drplantsmobileapp.RecyclerView.AddPlantRecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.drplantsmobileapp.R;

public class OptimalAttributeViewHolder extends RecyclerView.ViewHolder {

    private final ImageView attributeImage;
    private final TextView attributeOptimalValue;

    public OptimalAttributeViewHolder(@NonNull View itemView) {
        super(itemView);
        attributeImage = itemView.findViewById(R.id.icon_item);
        attributeOptimalValue = itemView.findViewById(R.id.text_item);
    }

    public ImageView getAttributeImage() {
        return this.attributeImage;
    }

    public TextView getAttributeOptimalValue() {
        return this.attributeOptimalValue;
    }
}
