package com.example.drplantsmobileapp.RecyclerView.DeviceRecyclerView;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "device")
public class Device {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "device_id")
    private int id;
    @ColumnInfo(name = "device_name")
    private final String name;
    @ColumnInfo(name = "plant_associated")
    private final int plantAssociated;
    @ColumnInfo(name = "device_connection")
    private final int idESP;


    public Device (String name, int plantAssociated, int idESP){
        this.name = name;
        this.plantAssociated = plantAssociated;
        this.idESP = idESP;
    }

    public String getName(){
        return this.name;
    }

    public int getId(){ return this.id;}

    public void setId(int id){ this.id = id; }

    public int getPlantAssociated() {
        return plantAssociated;
    }

    public int getIdESP() {
        return idESP;
    }

}
