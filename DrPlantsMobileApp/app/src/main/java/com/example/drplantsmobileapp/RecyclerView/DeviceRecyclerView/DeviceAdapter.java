package com.example.drplantsmobileapp.RecyclerView.DeviceRecyclerView;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.drplantsmobileapp.R;
import com.example.drplantsmobileapp.RecyclerView.OnItemListener;

import java.util.ArrayList;
import java.util.List;

public class DeviceAdapter extends RecyclerView.Adapter<DeviceViewHolder>{

    private List<Device> deviceList;
    private final Activity activity;
    private final OnItemListener listener;

    public DeviceAdapter(Activity activity, OnItemListener listener){
        this.deviceList = new ArrayList<>();
        this.activity = activity;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DeviceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new DeviceViewHolder(layoutView, this.listener);
    }

    @Override
    public void onBindViewHolder(@NonNull DeviceViewHolder holder, int position) {
        Device currentDevice = this.deviceList.get(position);

        holder.deviceName.setText(currentDevice.getName());
        holder.deviceIcon.setImageResource(R.drawable.ic_plant_device);

    }

    @Override
    public int getItemCount() {
        return deviceList.size();
    }

    public void setData(List<Device> list){
        this.deviceList = new ArrayList<>(list);
        notifyDataSetChanged();
    }

}
