package com.example.drplantsmobileapp.RecyclerView.DeviceRecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.drplantsmobileapp.R;
import com.example.drplantsmobileapp.RecyclerView.OnItemListener;

public class DeviceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{

    ImageView deviceIcon;
    TextView deviceName;
    private final OnItemListener listener;

    public DeviceViewHolder(@NonNull View itemView, OnItemListener listener) {
        super(itemView);
        deviceIcon =  itemView.findViewById(R.id.icon_item);
        deviceName = itemView.findViewById(R.id.text_item);
        this.listener = listener;

        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
    }

    @Override
    public void onClick(View v) {
        listener.onItemClick(getAdapterPosition());
    }

    @Override
    public boolean onLongClick(View v) {
        if (listener != null){
            return listener.onItemLongClick(getAdapterPosition());
        }
        return false;
    }
}
