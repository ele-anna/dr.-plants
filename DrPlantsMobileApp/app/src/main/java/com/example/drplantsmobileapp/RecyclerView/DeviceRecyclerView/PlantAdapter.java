package com.example.drplantsmobileapp.RecyclerView.DeviceRecyclerView;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.drplantsmobileapp.R;
import com.example.drplantsmobileapp.RecyclerView.PlantsRecyclerView.PlantsCardItem;
import com.google.android.material.checkbox.MaterialCheckBox;

import java.util.ArrayList;
import java.util.List;



public class PlantAdapter extends RecyclerView.Adapter<PlantViewHolder>{

    private List<PlantsCardItem> plantList;
    private final Activity activity;
    private MaterialCheckBox lastChecked;
    private int plantIdChecked;

    public PlantAdapter(Activity activity)
    {
        this.plantList = new ArrayList<>();
        this.activity = activity;
        this.lastChecked = null;
        this.plantIdChecked = 0;
    }

    private void setCheckBoxListener(PlantViewHolder holder){
        holder.checkbox.setOnClickListener((View v) ->{
            holder.checkbox.setChecked(false);
            if(this.lastChecked == null){
                this.lastChecked = holder.checkbox;
                this.plantIdChecked = holder.plantId;
                lastChecked.setChecked(true);
            }else{
                if(!this.lastChecked.equals(holder.checkbox)){
                    this.lastChecked.setChecked(false);
                    this.lastChecked = holder.checkbox;
                    this.plantIdChecked = holder.plantId;
                    lastChecked.setChecked(true);
                }

            }
        });
    }

    @NonNull
    @Override
    public PlantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.checkbox, parent, false);
        return new PlantViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull PlantViewHolder holder, int position) {
        PlantsCardItem currentPlant = plantList.get(position);

        holder.checkbox.setText(currentPlant.getPlantName());
        holder.plantId = currentPlant.getId();
        this.setCheckBoxListener(holder);
    }

    @Override
    public int getItemCount() {
        return plantList.size();
    }

    public void setData(List<PlantsCardItem> plants){
        this.plantList = new ArrayList<>(plants);
        notifyDataSetChanged();
    }

    public MaterialCheckBox getLastChecked() {
        return this.lastChecked;
    }

    public int getPlantIdChecked() {
        return this.plantIdChecked;
    }
}
