package com.example.drplantsmobileapp.RecyclerView.DeviceRecyclerView;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.drplantsmobileapp.R;
import com.google.android.material.checkbox.MaterialCheckBox;

public class PlantViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    MaterialCheckBox checkbox;
    int plantId;

    public PlantViewHolder(@NonNull View itemView) {
        super(itemView);
        this.checkbox = itemView.findViewById(R.id.checkbox);
    }


    @Override
    public void onClick(View v) {
    }
}
