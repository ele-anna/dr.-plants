package com.example.drplantsmobileapp.RecyclerView.NotificationsRecyclerView;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "notification")
public class Notification {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "notification_id")
    private int id;
    @ColumnInfo(name = "notification_text")
    private final String notificationText;
    @ColumnInfo(name = "notification_resource")
    private final String type;
    @ColumnInfo(name = "plant_associated")
    private final int plantAssociated;


    public Notification(String notificationText, String type, int plantAssociated) {
        this.notificationText = notificationText;
        this.type = type;
        this.plantAssociated = plantAssociated;
    }

    public String getType() {
        return this.type;
    }

    public String getNotificationText() {
        return this.notificationText;
    }

    public int getId(){ return this.id;}

    public void setId(int id){ this.id = id; }

    public int getPlantAssociated() {
        return plantAssociated;
    }
}
