package com.example.drplantsmobileapp.RecyclerView.NotificationsRecyclerView;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.drplantsmobileapp.R;
import com.example.drplantsmobileapp.RecyclerView.PlantattributeRecyclerView.AttributeItem;

import java.util.ArrayList;
import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationViewHolder> {

    private List<Notification> notificationList;
    private final Activity activity;

    public NotificationAdapter(Activity activity) {
        this.activity = activity;
        this.notificationList = new ArrayList<>();
    }

    @NonNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new NotificationViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationViewHolder holder, int position) {
        final Notification currentNotification = notificationList.get(position);
        final String type = currentNotification.getType();
        final String notificationImage = AttributeItem.valueOf(type).getAttributeImage();
        final Drawable drawable = ContextCompat.getDrawable(activity, activity.getResources().getIdentifier(notificationImage, "drawable", activity.getPackageName()));
        holder.getNotificationImage().setImageDrawable(drawable);
        holder.getNotificationText().setText(currentNotification.getNotificationText());
    }

    @Override
    public int getItemCount() {
        return this.notificationList.size();
    }

    public void setData(List<Notification> notificationList) {
        this.notificationList = notificationList;
        notifyDataSetChanged();
    }

    public Notification getNotification(int position){
        return this.notificationList.get(position);
    }
}
