package com.example.drplantsmobileapp.RecyclerView.NotificationsRecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.drplantsmobileapp.R;

public class NotificationViewHolder extends RecyclerView.ViewHolder {

    private final ImageView notificationImage;
    private final TextView notificationText;

    public NotificationViewHolder(@NonNull View itemView) {
        super(itemView);
        this.notificationText = itemView.findViewById(R.id.text_item);
        this.notificationImage = itemView.findViewById(R.id.icon_item);
    }

    public ImageView getNotificationImage() {
        return this.notificationImage;
    }

    public TextView getNotificationText() {
        return this.notificationText;
    }
}
