package com.example.drplantsmobileapp.RecyclerView.NotificationsRecyclerView;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.example.drplantsmobileapp.ViewModel.NotificationListViewModel;

public class SwipeToDelete extends ItemTouchHelper.SimpleCallback {

    private final NotificationAdapter adapter;
    private final NotificationListViewModel viewModel;

    public SwipeToDelete(NotificationAdapter adapter, NotificationListViewModel viewModel)
    {
        super(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
        this.adapter = adapter;
        this.viewModel = viewModel;
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
        int position = viewHolder.getAdapterPosition();
        Log.e("SWIPE", this.viewModel.toString());
        this.viewModel.deleteNotification(this.adapter.getNotification(position));
    }
}
