package com.example.drplantsmobileapp.RecyclerView;

public interface OnItemListener {

    void onItemClick(int position);

    boolean onItemLongClick(int position);

}
