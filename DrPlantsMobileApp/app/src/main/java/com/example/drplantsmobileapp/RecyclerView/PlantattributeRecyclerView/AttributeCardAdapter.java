package com.example.drplantsmobileapp.RecyclerView.PlantattributeRecyclerView;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;


import com.example.drplantsmobileapp.R;
import com.example.drplantsmobileapp.RecyclerView.OnItemListener;

import java.util.ArrayList;
import java.util.List;

public class AttributeCardAdapter extends RecyclerView.Adapter<AttributeCardViewHolder> {

    private List<AttributeItem> attributeItemList;
    private final Activity activity;
    private final OnItemListener listener;

    public AttributeCardAdapter(Activity activity, OnItemListener listener) {
        this.attributeItemList = new ArrayList<>();
        this.activity = activity;
        this.listener = listener;
    }

    @NonNull
    @Override
    public AttributeCardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.plant_attributes_layout, parent, false);
        return new AttributeCardViewHolder(layoutView, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull AttributeCardViewHolder holder, int position) {
        final AttributeItem currentAttributeItem = this.attributeItemList.get(position);
        final String attributeImage = currentAttributeItem.getAttributeImage();
        final Drawable drawable = ContextCompat.getDrawable(activity, activity.getResources().getIdentifier(attributeImage, "drawable", activity.getPackageName()));
        holder.getAttributeImage().setImageDrawable(drawable);
        holder.getAttributeName().setText(currentAttributeItem.getAttributeName());
    }

    @Override
    public int getItemCount() {
        return this.attributeItemList.size();
    }

    public void setData(List<AttributeItem> plantAttributeItems) {
        this.attributeItemList = plantAttributeItems;
        notifyDataSetChanged();
    }
}
