package com.example.drplantsmobileapp.RecyclerView.PlantattributeRecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.drplantsmobileapp.R;
import com.example.drplantsmobileapp.RecyclerView.OnItemListener;


public class AttributeCardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final TextView attributeName;
    private final ImageView attributeImage;
    private final OnItemListener onItemListener;

    public AttributeCardViewHolder(@NonNull View itemView, OnItemListener onItemListener) {
        super(itemView);
        this.onItemListener = onItemListener;
        attributeName = itemView.findViewById(R.id.card_attribute_name);
        attributeImage = itemView.findViewById(R.id.card_attribute_image);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        onItemListener.onItemClick(getAdapterPosition());
    }

    public TextView getAttributeName() {
        return attributeName;
    }

    public ImageView getAttributeImage() {
        return attributeImage;
    }
}
