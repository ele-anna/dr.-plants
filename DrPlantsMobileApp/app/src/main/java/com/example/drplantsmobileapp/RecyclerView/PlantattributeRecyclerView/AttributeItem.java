package com.example.drplantsmobileapp.RecyclerView.PlantattributeRecyclerView;

import android.graphics.Color;

import com.example.drplantsmobileapp.R;

public enum AttributeItem {

    BRIGHTNESS(R.string.brightness, "Lux", "@drawable/ic_sun", Color.YELLOW),

    HUMIDITY(R.string.humidity, "%", "@drawable/ic_humidity", Color.BLUE),

    AIR_QUALITY(R.string.air_quality, "PPM", "@drawable/ic_airquality", Color.GREEN),

    TEMPERATURE(R.string.temperature, "°", "@drawable/ic_thermometer", Color.RED);

    private final int attributeName;
    private final String unityOfMeasure;
    private final String attributeImage;
    private final int color;

    AttributeItem(int attributeName, String unityOfMeasure, String attributeImage, int color) {
        this.attributeName = attributeName;
        this.unityOfMeasure = unityOfMeasure;
        this.attributeImage = attributeImage;
        this.color = color;
    }

    public int getAttributeName() {
        return attributeName;
    }

    public String getAttributeImage() {
        return attributeImage;
    }

    public String getUnityOfMeasure() {
        return unityOfMeasure;
    }

    public int getColor() {
        return this.color;
    }
}
