package com.example.drplantsmobileapp.RecyclerView.PlantsRecyclerView;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.example.drplantsmobileapp.R;
import com.example.drplantsmobileapp.RecyclerView.OnItemListener;
import com.example.drplantsmobileapp.Utilities;

import java.util.ArrayList;
import java.util.List;

public class PlantsCardAdapter extends RecyclerView.Adapter<PlantsCardViewHolder> {

    private List<PlantsCardItem> cardItemList;
    private final Activity activity;
    private final OnItemListener listener;

    public PlantsCardAdapter(Activity activity, OnItemListener listener) {
        this.cardItemList = new ArrayList<>();
        this.activity = activity;
        this.listener = listener;
    }

    @NonNull
    @Override
    public PlantsCardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.plant_card_layout, parent, false);
        return new PlantsCardViewHolder(layoutView, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull PlantsCardViewHolder holder, int position) {
        final PlantsCardItem currentPlantsCardItem = cardItemList.get(position);
        final String plant_image_path = currentPlantsCardItem.getImageResource();
        if (plant_image_path.contains("ic_")) {
            final Drawable drawable = ContextCompat.getDrawable(activity, activity.getResources().getIdentifier(plant_image_path, "drawable", activity.getPackageName()));
            holder.getPlantImage().setImageDrawable(drawable);
        } else {
            final Bitmap bitmap = Utilities.getImageBitmap(activity, Uri.parse(plant_image_path));
            if (bitmap != null) {
                holder.getPlantImage().setImageBitmap(bitmap);
            }
        }
        holder.getPlantName().setText(currentPlantsCardItem.getPlantName());
        final int deviceAssociated = currentPlantsCardItem.getDate_association() != null ? View.VISIBLE : View.GONE;
        holder.getDeviceAssociated().setVisibility(deviceAssociated);
    }

    @Override
    public int getItemCount() {
        return cardItemList.size();
    }

    public void setData(List<PlantsCardItem> list) {
        this.cardItemList = list;
        notifyDataSetChanged();
    }
}
