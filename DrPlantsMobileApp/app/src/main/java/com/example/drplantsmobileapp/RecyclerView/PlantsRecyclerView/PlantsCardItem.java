package com.example.drplantsmobileapp.RecyclerView.PlantsRecyclerView;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "plant")
public class PlantsCardItem {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "plant_id")
    private int id;
    @ColumnInfo(name = "plant_name")
    private final String plantName;
    @ColumnInfo(name = "plant_image")
    private final String imageResource;
    @ColumnInfo(name = "plant_description")
    private final String plantDescription;
    @ColumnInfo(name = "date_of_association")
    private String date_association;

    public PlantsCardItem(String plantName, String imageResource, String plantDescription) {
        this.plantName = plantName;
        this.imageResource = imageResource;
        this.plantDescription = plantDescription;
        this.date_association = null;
    }

    public String getPlantName() {
        return this.plantName;
    }

    public String getImageResource() {
        return this.imageResource;
    }

    public String getPlantDescription() { return plantDescription; }

    public int getId() { return this.id; }

    public void setId(int id) { this.id = id; }

    public String getDate_association() {
        return date_association;
    }

    public void setDate_association(String date_assocation) {
        this.date_association = date_assocation;
    }
}
