package com.example.drplantsmobileapp.RecyclerView.PlantsRecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.drplantsmobileapp.R;
import com.example.drplantsmobileapp.RecyclerView.OnItemListener;

public class PlantsCardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    private final TextView plantName;
    private final ImageView deviceAssociated;
    private final ImageView plantImage;
    private final OnItemListener itemListener;

    public PlantsCardViewHolder(@NonNull View itemView, OnItemListener itemListener) {
        super(itemView);
        this.plantName = itemView.findViewById(R.id.card_plant_name);
        this.deviceAssociated = itemView.findViewById(R.id.card_plant_with_device);
        this.plantImage = itemView.findViewById(R.id.card_plant_image);
        this.itemListener = itemListener;

        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
    }

    @Override
    public void onClick(View v) {
        itemListener.onItemClick(getAdapterPosition());
    }

    public TextView getPlantName() {
        return this.plantName;
    }

    public ImageView getDeviceAssociated() {
        return this.deviceAssociated;
    }

    public ImageView getPlantImage() {
        return this.plantImage;
    }

    @Override
    public boolean onLongClick(View v) {
        if (itemListener != null){
            return itemListener.onItemLongClick(getAdapterPosition());
        }
        return false;
    }
}
