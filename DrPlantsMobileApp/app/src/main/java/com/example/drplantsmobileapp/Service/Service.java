package com.example.drplantsmobileapp.Service;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.drplantsmobileapp.DataBase.DetectionRepository;
import com.example.drplantsmobileapp.DataBase.DeviceRepository;
import com.example.drplantsmobileapp.DataBase.NotificationRepository;
import com.example.drplantsmobileapp.Detection;
import com.example.drplantsmobileapp.R;
import com.example.drplantsmobileapp.RecyclerView.DeviceRecyclerView.Device;
import com.example.drplantsmobileapp.RecyclerView.NotificationsRecyclerView.Notification;
import com.example.drplantsmobileapp.RecyclerView.PlantattributeRecyclerView.AttributeItem;
import com.example.drplantsmobileapp.Utilities;

import org.json.JSONException;

import java.sql.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.example.drplantsmobileapp.Utilities.ADDRESS;

public class Service extends android.app.Service {

    private final static String OSM_REQUEST_TAG = "OSM_REQUEST";
    private final static String TAG = "Service";
    private final static String CHANNEL_ID = "channel0";
    private static final int NOTIFY = 4000;

    private RequestQueue requestQueue;
    private DeviceRepository deviceRepository;
    private DetectionRepository detectionRepository;
    private NotificationRepository notificationRepository;
    private Timer timer = null;
    private int plantAssociated;
    private String plantName = "";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        this.requestQueue = Volley.newRequestQueue(getApplication());
        this.deviceRepository = new DeviceRepository(getApplication());
        this.detectionRepository = new DetectionRepository(getApplication());
        this.notificationRepository = new NotificationRepository(getApplication());

        if(this.timer != null){
            this.timer.cancel();
        }else{
           this.timer = new Timer();
        }

        this.timer.scheduleAtFixedRate(new TimeDisplay(), 0, NOTIFY);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }

    class TimeDisplay extends TimerTask {

        @Override
        public void run() {
            new Handler(Looper.getMainLooper()).post(Service.this::sendVolleyRequest);
        }
    }

    private void sendVolleyRequest() {
        this.deviceRepository.getDeviceList().observeForever((List<Device> deviceList) -> {

            for (Device device : deviceList) {
                String url = ADDRESS + "/api/data/?id=" + device.getIdESP();

                // Request a jsonObject response from the provided URL.
                JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url,
                        null, response -> {
                    Log.d(TAG, response.toString());

                    for (int i = 0; i < response.length(); i++) {
                        try {
                            String type = response.getJSONObject(i).get("type").toString();
                            String task = response.getJSONObject(i).get("task").toString();
                            this.deviceRepository.getPlantAssociated(device.getId()).observeForever(plantAndDevice -> {
                                plantAssociated = plantAndDevice.plant.getId();
                                plantName = plantAndDevice.plant.getPlantName();
                            });

                            if (type.equals("DATA")) {
                                float value = Float.parseFloat(response.getJSONObject(i).get("value").toString());
                                String unitOfMeasure = AttributeItem.valueOf(task).getUnityOfMeasure();
                                this.detectionRepository.addDetection(new Detection(task, new Date(System.currentTimeMillis()).toString(), value, unitOfMeasure, this.plantAssociated));
                            }else{
                                String value = response.getJSONObject(i).get("value").toString();
                                String message = "";
                                if(value.startsWith("N")){
                                    message = this.plantName + ": " + getString(R.string.parameter) + getString(AttributeItem.valueOf(task).getAttributeName()) + getString(R.string.normal_notification);
                                } else{

                                    if (value.equals("low")){
                                        switch(AttributeItem.valueOf(task)) {
                                            case BRIGHTNESS:
                                                message = getString(R.string.your_plant) + this.plantName + getString(R.string.brightness_low);
                                                break;
                                            case HUMIDITY:
                                                message = getString(R.string.your_plant) + this.plantName + getString(R.string.humidity_low);
                                                break;
                                            case TEMPERATURE:
                                                message = getString(R.string.temperature_low) + this.plantName + "!";
                                                break;
                                        }
                                    } else {
                                        switch(AttributeItem.valueOf(task)) {
                                            case BRIGHTNESS:
                                                message = getString(R.string.your_plant) + this.plantName + getString(R.string.brightness_high);
                                                break;
                                            case HUMIDITY:
                                                message = getString(R.string.your_plant) + this.plantName + getString(R.string.humidity_high);
                                                break;
                                            case TEMPERATURE:
                                                message = getString(R.string.temperature_high) + this.plantName + "!";
                                                break;
                                            case AIR_QUALITY:
                                                message = getString(R.string.air_quality_high) + this.plantName + getString(R.string.air_quality_high_2);
                                                break;
                                        }
                                        }
                                    }
                                this.notificationRepository.addNotification(new Notification(message, task, plantAssociated));

                                    this.createHeadUpNotification(message, task);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, error -> Log.d(TAG, error.toString()));

                jsonArrayRequest.setTag(OSM_REQUEST_TAG);
                // Add the request to the RequestQueue.
                requestQueue.add(jsonArrayRequest);
            }
        });

    }

    private void createHeadUpNotification(String message, String task) {
        int icon = R.mipmap.ic_launcher_foreground;
        final Bitmap drawable = Utilities.drawableToBitmap(ContextCompat.getDrawable(getApplication(), getApplication().getResources().getIdentifier(AttributeItem.valueOf(task).getAttributeImage(), "drawable", getApplication().getPackageName())));

        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N | Build.VERSION.SDK_INT == Build.VERSION_CODES.N_MR1) {
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
            notificationBuilder
                    .setSmallIcon(icon)
                    .setLargeIcon(drawable)
                    .setContentTitle("Running")
                    .setCategory(NotificationCompat.CATEGORY_SERVICE)
                    .setWhen(System.currentTimeMillis())
                    .setTicker("Running")
                    .setOngoing(true)
                    .build();
        }

            NotificationManager notificationManager = (NotificationManager)       getSystemService(Context.NOTIFICATION_SERVICE);
            String NOTIFICATION_CHANNEL_ID = "tutorialspoint_01";
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_MAX);
                // Configure the notification channel.
                notificationChannel.setDescription(message);
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.RED);
                notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
                notificationChannel.enableVibration(true);
                notificationManager.createNotificationChannel(notificationChannel);
            }
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
            notificationBuilder.setAutoCancel(true)
                    .setDefaults(android.app.Notification.DEFAULT_ALL)
                    .setWhen(System.currentTimeMillis())
                    .setSmallIcon(icon)
                    .setLargeIcon(drawable)
                    .setTicker(getString(R.string.app_name))
                    //.setPriority(Notification.PRIORITY_MAX)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(message)
                    .setContentInfo("Information");
            notificationManager.notify(1, notificationBuilder.build());
    }
}
