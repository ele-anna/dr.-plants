package com.example.drplantsmobileapp.ViewModel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


import com.example.drplantsmobileapp.DataBase.DetectionRepository;
import com.example.drplantsmobileapp.DataBase.DeviceRepository;
import com.example.drplantsmobileapp.DataBase.OptimalAttributeRepository;
import com.example.drplantsmobileapp.DataBase.PlantAndDevice;
import com.example.drplantsmobileapp.Detection;
import com.example.drplantsmobileapp.RecyclerView.AddPlantRecyclerView.OptimalAttributeValue;
import com.example.drplantsmobileapp.RecyclerView.DeviceRecyclerView.Device;
import com.example.drplantsmobileapp.RecyclerView.PlantsRecyclerView.PlantsCardItem;

import java.util.List;

public class DeviceViewModel extends AndroidViewModel {

    private final LiveData<List<Device>> devices;
    private final MutableLiveData<Device> itemSelected = new MutableLiveData<>();
    private final DeviceRepository deviceRepository;
    private final OptimalAttributeRepository optimalAttributeRepository;
    private final DetectionRepository detectionRepository;

    public DeviceViewModel(@NonNull Application application) {
        super(application);
        this.deviceRepository = new DeviceRepository(application);
        this.detectionRepository = new DetectionRepository(application);
        this.optimalAttributeRepository = new OptimalAttributeRepository(application);
        this.devices = deviceRepository.getDeviceList();
    }

    public void select(Device device){
        this.itemSelected.setValue(device);
    }

    public LiveData<Device> getSelected(){
        return this.itemSelected;
    }

    public LiveData<List<Device>> getDevices(){
        return this.devices;
    }

    public Device getDevice(int position){
        return devices.getValue() == null ? null : this.devices.getValue().get(position);
    }

    public void addDevice(Device device) {
        this.deviceRepository.addDevice(device);
    }

    public void associateDevice(int plantId, String date) {
        this.deviceRepository.associateDevice(plantId, date);
    }

    public LiveData<List<PlantsCardItem>> getPlantsWithoutDevice(){
        return this.deviceRepository.getPlantsWithoutDevice();
    }

    public void updateDeviceName(String name, int id){
        this.deviceRepository.updateDeviceName(name, id);
    }

    public void updatePlantAssociated(int deviceId, int plantId){
        this.deviceRepository.updatePlantAssociated(deviceId, plantId);
    }


    public LiveData<PlantAndDevice> getPlantAssociated(int deviceId){
        return  this.deviceRepository.getPlantAssociated(deviceId);
    }

    public LiveData<List<Detection>> getDetectionsByPlantId(int plantId) {
        return this.detectionRepository.getDetectionListByPlantId(plantId);
    }

    public void deleteDevice(Device device, int plantAssociated) {
        this.deviceRepository.deleteDevice(device, plantAssociated);
    }

    public boolean checkDeviceConnection(int deviceEsp) {
        Log.e("MODEL", deviceEsp + "");
        List<Device> conn = this.deviceRepository.getConnection(deviceEsp);
        boolean test = conn == null;
        Log.e("MODEL", String.valueOf(test));
        return conn == null;
    }

    public List<OptimalAttributeValue> getPlantOptimalAttributeList(int plantId) {
        return this.optimalAttributeRepository.getPlantOptimalAttribute(plantId);
    }
}
