package com.example.drplantsmobileapp.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.drplantsmobileapp.DataBase.OptimalAttributeRepository;
import com.example.drplantsmobileapp.RecyclerView.AddPlantRecyclerView.OptimalAttributeValue;

public class OptimalAttributeModel extends AndroidViewModel {

    private final OptimalAttributeRepository optimalAttributeRepository;

    public OptimalAttributeModel(@NonNull Application application) {
        super(application);
        this.optimalAttributeRepository = new OptimalAttributeRepository(application);
    }

    public LiveData<OptimalAttributeValue> getSpecificOptimalAttributeValue(int plantId, String attributeType) {
        return this.optimalAttributeRepository.getSpecificOptimalAttribute(plantId, attributeType);
    }
}
