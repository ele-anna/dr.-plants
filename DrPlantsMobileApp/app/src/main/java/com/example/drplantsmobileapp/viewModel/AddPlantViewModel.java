package com.example.drplantsmobileapp.ViewModel;

import android.app.Application;
import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.drplantsmobileapp.DataBase.OptimalAttributeRepository;
import com.example.drplantsmobileapp.DataBase.PlantRepository;
import com.example.drplantsmobileapp.RecyclerView.AddPlantRecyclerView.OptimalAttributeValue;
import com.example.drplantsmobileapp.RecyclerView.PlantsRecyclerView.PlantsCardItem;

import java.util.List;

public class AddPlantViewModel extends AndroidViewModel {

    private final MutableLiveData<Bitmap> imageBitmap;
    private final PlantRepository plantRepository;
    private final OptimalAttributeRepository optimalAttributeRepository;
    private long plantId;

    public AddPlantViewModel(@NonNull Application application) {
        super(application);
        this.imageBitmap = new MutableLiveData<>();
        this.plantRepository = new PlantRepository(application);
        this.optimalAttributeRepository = new OptimalAttributeRepository(application);
    }

    public void setImageBitmap(Bitmap bitmap) {
        this.imageBitmap.setValue(bitmap);
    }

    public LiveData<Bitmap> getImageBitmap() {
        return this.imageBitmap;
    }

    public void addPlantItem(PlantsCardItem plantsCardItem, List<OptimalAttributeValue> list) {
        this.plantId = this.plantRepository.addPlant(plantsCardItem);
        for (OptimalAttributeValue attribute : list) {
            attribute.setPlantAssociated(plantId);
            this.optimalAttributeRepository.addOptimalAttribute(attribute);
        }
    }
}
