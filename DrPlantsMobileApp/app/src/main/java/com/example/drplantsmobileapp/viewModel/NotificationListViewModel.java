package com.example.drplantsmobileapp.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.drplantsmobileapp.DataBase.NotificationRepository;
import com.example.drplantsmobileapp.RecyclerView.NotificationsRecyclerView.Notification;

import java.util.List;

public class NotificationListViewModel extends AndroidViewModel {

    private final NotificationRepository notificationRepository;

    public NotificationListViewModel(@NonNull Application application) {
        super(application);
        this.notificationRepository = new NotificationRepository(application);
    }

    public LiveData<List<Notification>> getNotifications() {
        return this.notificationRepository.getNotificationList();
    }

    public void deleteNotification(Notification notification) {
        this.notificationRepository.deleteNotification(notification);
    }
}
