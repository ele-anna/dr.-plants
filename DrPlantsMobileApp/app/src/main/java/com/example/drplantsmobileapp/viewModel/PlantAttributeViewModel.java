package com.example.drplantsmobileapp.ViewModel;

import android.app.Application;
import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.drplantsmobileapp.DataBase.DetectionRepository;
import com.example.drplantsmobileapp.DataBase.OptimalAttributeRepository;
import com.example.drplantsmobileapp.Detection;
import com.example.drplantsmobileapp.RecyclerView.AddPlantRecyclerView.OptimalAttributeValue;
import com.example.drplantsmobileapp.RecyclerView.PlantattributeRecyclerView.AttributeItem;
import com.example.drplantsmobileapp.RecyclerView.PlantsRecyclerView.PlantsCardItem;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class PlantAttributeViewModel extends AndroidViewModel {

    private MutableLiveData<List<AttributeItem>> attributeItems;
    private final MutableLiveData<Pair<PlantsCardItem, AttributeItem>> itemSelected;
    private final DetectionRepository detectionRepository;
    private final OptimalAttributeRepository optimalAttributeRepository;
    private final MutableLiveData<Map<AttributeItem, String>> detectionState;

    public PlantAttributeViewModel(@NonNull Application application) {
        super(application);
        this.detectionRepository = new DetectionRepository(application);
        this.optimalAttributeRepository = new OptimalAttributeRepository(application);
        this.itemSelected = new MutableLiveData<>();
        this.detectionState = new MutableLiveData<>();
        if (itemSelected.getValue() == null) {
            itemSelected.setValue(new Pair(null, null));
        }
    }

    public void selectAttribute(AttributeItem attributeItem) {
        final PlantsCardItem plant = this.itemSelected.getValue().first;
        itemSelected.setValue(new Pair(plant, attributeItem));
    }

    public LiveData<Pair<PlantsCardItem, AttributeItem>> getSelectedPlantAndAttribute() {
        return this.itemSelected;
    }

    public LiveData<List<AttributeItem>> getAttributeItems() {
        if (this.attributeItems == null) {
            this.attributeItems = new MutableLiveData<>();
            final List<AttributeItem> attributeItemList = Arrays.asList(AttributeItem.values());
            this.attributeItems.setValue(attributeItemList);
        }
        return this.attributeItems;
    }

    public LiveData<List<Detection>> getDetectionList() {
        return this.detectionRepository.getDetectionList(this.itemSelected.getValue().first.getId(), this.itemSelected.getValue().second.name());
    }

    public LiveData<OptimalAttributeValue> getOptimalValue() {
        return this.optimalAttributeRepository.getSpecificOptimalAttribute(this.itemSelected.getValue().first.getId(), this.itemSelected.getValue().second.name());
    }

    public AttributeItem getAttributeItem(int position) {
        return this.attributeItems.getValue() == null ? null : this.attributeItems.getValue().get(position);
    }

    public void setCurrentPlant(final PlantsCardItem plant) {
        final AttributeItem attributeItem = this.itemSelected.getValue().second;
        itemSelected.setValue(new Pair(plant, attributeItem));
    }

}
