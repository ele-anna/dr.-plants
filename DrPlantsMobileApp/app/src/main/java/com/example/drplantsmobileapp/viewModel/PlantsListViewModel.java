package com.example.drplantsmobileapp.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.drplantsmobileapp.DataBase.PlantRepository;
import com.example.drplantsmobileapp.RecyclerView.PlantsRecyclerView.PlantsCardItem;

import java.util.List;

public class PlantsListViewModel extends AndroidViewModel {

    private final LiveData<List<PlantsCardItem>> plantsCardItems;
    private final MutableLiveData<PlantsCardItem> itemSelected;
    private final PlantRepository plantRepository;

    public PlantsListViewModel(@NonNull Application application) {
        super(application);
        this.itemSelected = new MutableLiveData<>();
        this.plantRepository = new PlantRepository(application);
        this.plantsCardItems = plantRepository.getPlantList();
    }

    public void select(PlantsCardItem plantsCardItem) {
        itemSelected.setValue(plantsCardItem);
    }

    public LiveData<PlantsCardItem> getSelected() {
        return this.itemSelected;
    }

    public LiveData<List<PlantsCardItem>> getPlantsCardItems() {
        return this.plantsCardItems;
    }

    public PlantsCardItem getPlantsCardItem(int position) {
        return  plantsCardItems.getValue() == null ? null : plantsCardItems.getValue().get(position);
    }

    public void deletePlant(PlantsCardItem plant)
    {
        this.plantRepository.deletePlant(plant);
    }
}
