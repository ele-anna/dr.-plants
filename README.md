# Dr. Plants #

### Autori ###
Vitali Anna e Yan Elena

## Descrizione del progetto ##

L'applicazione Dr. Plants, consente tramite un apposito dispositivo, di poter monitorare lo stato di salute delle proprie piante. Tale applicazione risulta, essere ideale per utenti appassionati di giardinaggio o che possiedono magari il cosiddetto "pollice nero", in quanto li consentirà di gestire meglio il proprio orto o giardino. ​

In particolare tramite l'utilizzo di quest'applicazione si potrà:​

  - Riconoscere una pianta tramite lo scatto di una foto​

  - Associare un dispositivo di rilevazione a una pianta, con la possibilità in futuro di cambiare la pianta associata, senza perdere i valori precedenti​

  - Avere aggiornamenti in tempo reale sullo stato di salute della pianta, potendo visionare tutti i valori rilevati​

  - Ricevere notifiche in caso di valori critici

## Configurazione della rete ##
Nella cartella DrPlantsEsp, nel file _DrPlantsEsp.ino_ occorre impostare le variabili **SSIDNAME** con il nome della rete che si intende utilizzare e **PWD** con la password di tale rete, per consentire la connessione dell' ESP. Infine, in **ADDRESS** occorre inserire l'indirizzo del server dell'applicazione, per tale progetto è stato utilizzato un server _ngrok_, pertanto si consiglia di effettuare lo stesso.

Nella cartella DrPlantsMobileApp, occorre aprire il progetto Android e settare nella classe _Utilities.java_ il campo **ADDRESS** con l'indirizzo del server, anche in questo caso si consiglia di utilizzare _ngrok_.

## Come eseguire il progetto ##

1. Per prima cosa, se si è deciso di utilizzare il Server ngrok, occorre lanciare il servizio, dall'apposito prompt
2. Successivamente, occorre aprire la cartella DrPlantsEsp e lanciare il programma _DrPlantsEsp.ino_
3. Poi bisogna aprire la cartella DrPlantsArduino ed eseguire il programma _DrPlantsArduino.ino_
4. Dopodichè si puo procedere all'avvio del server, spostandosi nella cartella DrPlantsServer eseguendo il programma dalla classe _RunService.java_
5. Infine si potrà scaricare ed installare l'applicazione sul telefono, aprendo la cartella DrPlantsMobileApp e il rispettivo progetto Android, mettendolo in esecuzione sul device
